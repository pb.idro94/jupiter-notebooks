import requests
import json
from itertools import groupby

def request(url, api_key):
    url_final = url+'&api_key='+api_key
    #print(url_final)
    method = "GET"
    headers = {
        'Cache-Control': "no-cache",
        'Postman-Token': "7b133ebc-43a7-4d82-b410-ef77501103f9,fb646df0-5102-4024-9743-2d141189081c",
        }

    response = requests.request(method, url_final, headers=headers)
    return json.loads(response.text)

data = request("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000", "1DGpz3hekeC3QYvPhGbNkeivYtSj1QsZf3fW7GDp")
#print(data)

def build_web_page(data):
    html = ""
    html_ini = "<html>\n<head>\n</head>\n<body>\n"
    html_fin = "</body>\n</html>"
    html_img = ""
    for photo in data["photos"]:
        html_img += "<ul>\n\t<li><img src='" +  photo["img_src"] + "'></li>\n</ul>\n"
    html = html_ini + html_img + html_fin
    with open("output.html", "w") as f:
        f.write(html)

def photos_count(data):
    camaras = []
    salida = {}
    for photo in data["photos"]:
        camaras.append(photo["camera"]["name"])
    salida = {k: len(list(g)) for k, g in groupby(camaras)}
    return salida

data = request("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=100&page=1", "1DGpz3hekeC3QYvPhGbNkeivYtSj1QsZf3fW7GDp")
#print(data)

build_web_page(data)

print(photos_count(data))
