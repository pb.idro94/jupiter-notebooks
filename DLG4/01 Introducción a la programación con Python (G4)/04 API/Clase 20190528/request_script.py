import requests
import json

url = "https://jsonplaceholder.typicode.com/posts"

headers = {
    'User-Agent': "PostmanRuntime/7.13.0",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "bc52433f-70fd-4781-972c-6d1cbc17004c,74daa0d3-1ce9-4990-be9f-6a0316cfba45",
    'Host': "jsonplaceholder.typicode.com",
    'cookie': "__cfduid=dba979e11c6770da63c264cb889a761dd1558913737",
    'accept-encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, headers=headers)

results = json.loads(response.text)

print(type(results))
print(results[0])
