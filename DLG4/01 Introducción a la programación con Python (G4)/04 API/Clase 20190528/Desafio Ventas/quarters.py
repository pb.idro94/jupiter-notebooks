ventas = {
    "Enero": 15000,
    "Febrero": 22000,
    "Marzo": 12000,
    "Abril": 17000,
    "Mayo": 81000,
    "Junio": 13000,
    "Julio": 21000,
    "Agosto": 41200,
    "Septiembre": 25000,
    "Octubre": 21500,
    "Noviembre": 91000,
    "Diciembre": 21000,
}

i=0
q=1
suma=0
quarters = {}

for k, v in ventas.items():
    suma+=v
    i+=1
    #print(k, v)
    if i>=3:
        quarters["Q"+str(q)] = suma
        i=0
        q+=1
        suma=0

print(quarters)
