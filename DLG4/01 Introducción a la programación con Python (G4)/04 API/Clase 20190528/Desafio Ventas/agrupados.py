from itertools import groupby

ventas = {
    "Enero": 15000,
    "Febrero": 15000,
    "Marzo": 15000,
    "Abril": 17000,
    "Mayo": 17000,
    "Junio": 15000,
    "Julio": 17000,
    "Agosto": 16200,
    "Septiembre": 15000,
    "Octubre": 17500,
    "Noviembre": 17000,
    "Diciembre": 17000,
}

def grupos(ventas):
    group = {}
    valores = []
    for k, v in ventas.items():
        valores.append(v)
    valores.sort()
    group = {k: len(list(v)) for k,v in groupby(valores)}
    return group

print(grupos(ventas))
