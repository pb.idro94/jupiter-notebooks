import requests
import json

def request(method, url, payload = ""):
    #url = "https://reqres.in/api/users"

    headers = {
        'User-Agent': "PostmanRuntime/7.13.0",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Postman-Token': "7b133ebc-43a7-4d82-b410-ef77501103f9,fb646df0-5102-4024-9743-2d141189081c",
        'Host': "reqres.in",
        'cookie': "__cfduid=d2510924390edd6d585ddf35b26146ecb1559090068",
        'accept-encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }

    response = requests.request(method, url, data=payload)
    if method == "DELETE":
        return response
    else:
        return json.loads(response.text)

data = request("GET", "https://reqres.in/api/users")

print(data)

payload = {
    "name": "Hugo",
    "job": "leader"
    }

new = request("POST", "https://reqres.in/api/users")
print(new)

payload = {
    "name": "Andres",
    "job": "leader"
    }

upd = request("PUT", "https://reqres.in/api/users/1")
print(upd)

borra = request("DELETE", "https://reqres.in/api/users/1")
print(borra)
