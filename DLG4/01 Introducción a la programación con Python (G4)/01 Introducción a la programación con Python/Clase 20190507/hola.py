import sys

a = True
nombre = "Hugo"

print("Se han introducido {} parámetros".format(len(sys.argv)))

if len(sys.argv) > 0:
    print("El parámetro {} es de tipo {}".format(sys.argv[1],type(sys.argv[1])))

print(a)

print("Mi {} se llama {}".format("gato","Lola"))

#metodo nativo
print(len(nombre))

#metodo asociado a un objeto
print(nombre.upper())

lista = []

lista.append("perro")

print(lista)

lista.append("gato")

print(lista)
