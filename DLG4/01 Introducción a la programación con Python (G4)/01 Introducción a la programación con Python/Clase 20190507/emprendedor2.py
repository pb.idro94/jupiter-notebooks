import sys
import math

precio = float(sys.argv[1])
u_totales = int(sys.argv[2])
u_premium = int(sys.argv[3])
u_gratuitos = int(sys.argv[4])
gastos = float(sys.argv[5])

#calculo usuarios normales
u_normales = u_totales - (u_premium + u_gratuitos)

#calculo la utilidad inicial sin impuesto
ui = (2*precio*u_premium) + (precio*u_normales) - gastos

#calculo utilidad final con impuesto
uf = ui - (ui * 0.35)

if ui > 0:
	print (uf)
else:
	print (ui)
