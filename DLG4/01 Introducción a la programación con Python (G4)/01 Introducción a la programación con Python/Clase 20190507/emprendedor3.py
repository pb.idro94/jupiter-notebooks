import sys
import math

precio = float(sys.argv[1])
usuarios = int(sys.argv[2])
gastos = float(sys.argv[3])

#guardo la cantidad de argumentos
cant_arg = len(sys.argv)

if cant_arg > 4:
	u_ant = float(sys.argv[4])
else:
	u_ant = 1000

#calculo la utilidad inicial sin impuesto
ui = (precio * usuarios) - gastos
ui_ant = ui + u_ant

#calculo utilidad final con impuesto
uf = ui - (ui * 0.35) + u_ant

if ui > 0:
	print (uf)
else:
	print (ui_ant)