#Desafio bonus 2
nota = float(input("Ingrese una nota: "))

if nota > 7:
    print("Nota inválida.")
else:
    if nota <= 2.3:
        gringa = "F"
    elif nota <= 3.4:
        gringa = "D"
    elif nota <= 3.8:
        gringa = "C-"
    elif nota <= 4.3:
        gringa = "C+"
    elif nota <= 5.3:
        gringa = "B-"
    elif nota <= 5.8:
        gringa = "B"
    elif nota <= 6.4:
        gringa = "B+"
    elif nota <= 6.8:
        gringa = "A-"
    else:
        gringa = "A+"
    print("La nota en USA sería {}".format(gringa))
