#Desafio Bonus Año bisiesto
import sys

anio = int(sys.argv[1])

if anio%100==0 and anio%4!=0:
    r=False
elif anio%4==0 and anio%400==0:
    r=True
else:
    r=False

if r:
    print("El año {} es un año bisiesto.".format(anio))
else:
    print("El año {} no es un año bisiesto.".format(anio))
