#Desafio el mayor de tres numeros
import sys

if len(sys.argv)>3:

    #Asigno los valores de los argumentos
    num1 = int(sys.argv[1])
    num2 = int(sys.argv[2])
    num3 = int(sys.argv[3])

    #Defino la lógica
    if num1 > num2 and num1 > num3:
        resultado = num1
    elif num2 > num1 and num2 > num3:
        resultado = num2
    elif num3 > num1 and num3 > num2:
        resultado = num3
    elif num1 == num2 and num1 == num3:
        resultado = num1
    elif num1 == num2 and num1 > num3:
        resultado = num1
    elif num1 == num2 and num3 > num1:
        resultado = num3
    elif num1 == num3 and num1 > num2:
        resultado = num1
    else:
        resultado = num2

    #Imprimo el resultado
    print(resultado)
else:
    print("Error: Debe ingresar tres valores numéricos")
