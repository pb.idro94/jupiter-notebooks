#Desafio Piedra, Papel o Tijeras
import sys
import random

#Defino el juego del computador
juego_computador = int(random.randrange(3))

#Modifico el valor entero en un string
if juego_computador == 1:
    juego_computador = 'piedra'
elif juego_computador == 2:
    juego_computador = 'papel'
else:
    juego_computador = 'tijera'

#Valido que se haya ingresado un argumento correcto
if len(sys.argv) > 1:
    #Asigno el juego del usuario
    juego_usuario = str(sys.argv[1])
    #Valido el juego del usuario
    if juego_usuario == 'piedra' or juego_usuario == 'papel' or juego_usuario == 'tijera':
        #Defino la logica para el ganador
        if juego_usuario == 'piedra' and juego_computador == 'piedra':
            resultado = 'Empataste'
        elif juego_usuario == 'piedra' and juego_computador == 'papel':
            resultado = 'Perdiste'
        elif juego_usuario == 'piedra' and juego_computador == 'tijera':
            resultado = 'Ganaste'
        elif juego_usuario == 'papel' and juego_computador == 'piedra':
            resultado = 'Ganaste'
        elif juego_usuario == 'papel' and juego_computador == 'papel':
            resultado = 'Empataste'
        elif juego_usuario == 'papel' and juego_computador == 'tijera':
            resultado = 'Perdiste'
        elif juego_usuario == 'tijera' and juego_computador == 'piedra':
            resultado = 'Perdiste'
        elif juego_usuario == 'tijera' and juego_computador == 'papel':
            resultado = 'Ganaste'
        elif juego_usuario == 'tijera' and juego_computador == 'tijera':
            resultado = 'Empataste'
        #Imprimo el resultado
        print("Computador juega {}\n{}".format(juego_computador, resultado))
    else:
        print("Argumento inválido: Debe ser piedra, papel o tijera.")
else:
    print("Argumento inválido: Debe ser piedra, papel o tijera.")
