def letra_o(n):
    c = ""
    for i in range(n):
        for j in range(n):
            if i==0 or j==0 or j==n-1 or i==n-1:
                c+="*"
            else:
                c+=" "
        c+="\n"
    return c[0:len(c)-1]

print(letra_o(5))
