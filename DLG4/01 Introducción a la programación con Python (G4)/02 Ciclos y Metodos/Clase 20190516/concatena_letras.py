import string

def gen(n):
    s = str(string.ascii_lowercase)
    r = s[0:n]
    return r

print(gen(5))
