def depositar(saldo, cantidad):
    return saldo+cantidad

def girar(saldo, cantidad):
    if cantidad > saldo:
        return False
    else:
        return saldo-cantidad

def mostrar_menu(saldo = 0):
    m = "¡Bienvenido al Banco Amigo!. Escoja una opción:\n1. Consultar saldo\n2. Hacer depósito\n3. Realizar giro\n4. Salir"
    inv = "Opción inválida. Por favor ingrese 1, 2, 3 ó 4."
    menu = 0
    op = ""
    while menu!=4:
        giro = False
        print(m)
        op = input()
        if op.isnumeric():
            menu = int(op)
            if menu==1:
                print("Su saldo es de {}\n\n".format(saldo))
            elif menu==2:
                saldo = depositar(saldo, int(input()))
                print("Su saldo es de {}\n\n".format(saldo))
            elif menu==3:
                if saldo==0:
                    print("No puede realizar giros. Su saldo es {}".format(saldo))
                else:
                    while giro is False:
                        giro = girar(saldo, int(input()))
                        if giro is False:
                            print("No se puede girar esta cantidad. Su saldo es de {}.".format(saldo))
                    saldo = giro
                    print("Su saldo es de {}\n\n".format(saldo))
            elif menu==4:
                break
            else:
                print(inv)
        else:
            print("Opción inválida. Por favor ingrese 1, 2, 3 ó 4.")
            menu = 0

mostrar_menu(1000)
