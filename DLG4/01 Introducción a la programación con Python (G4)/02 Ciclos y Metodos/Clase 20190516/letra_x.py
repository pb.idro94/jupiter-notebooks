def letra_x(n):
    c = ""
    for i in range(n):
        for j in range(n):
            if i==j or i==n-j-1:
                c+="*"
            else:
                c+=" "
        c+="\n"
    return c[0:len(c)-1]

print(letra_x(5))
