op = ""
while op!="5":
    print("Calculadora\n1: Sumar\n2: Restar\n3: Multiplicar\n4: Dividir\n5: Salir")
    op = input("Ingrese opción: ")
    if op == "1":
        n1 = float(input("Ingrese el primero número: "))
        n2 = float(input("Ingrese el segundo número: "))
        r = n1+n2
        print("El resultado es: {}".format(r))
    elif op == "2":
        n1 = float(input("Ingrese el primero número: "))
        n2 = float(input("Ingrese el segundo número: "))
        r = n1-n2
        print("El resultado es: {}".format(r))
    elif op == "3":
        n1 = float(input("Ingrese el primero número: "))
        n2 = float(input("Ingrese el segundo número: "))
        r = n1*n2
        print("El resultado es: {}".format(r))
    elif op == "4":
        n1 = float(input("Ingrese el primero número: "))
        n2 = float(input("Ingrese el segundo número: "))
        r = n1/n2
        print("El resultado es: {}".format(r))
