import sys
n = int(sys.argv[1])
lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pulvinar ipsum eget imperdiet euismod. Nulla pulvinar auctor nulla, id tincidunt odio. Vestibulum quis dictum elit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam porta dolor tortor. Fusce purus enim, tempus nec lorem vel, bibendum molestie nulla. Ut sit amet sagittis tellus."
if n>0:
    salida = ""
    for i in range(n):
        salida+=lorem
        if i<n-1:
            salida+="\n\n"
    print(salida)
