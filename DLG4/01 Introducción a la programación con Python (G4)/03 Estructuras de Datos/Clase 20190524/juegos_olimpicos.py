import pandas as pd

df = pd.read_csv("athlete_events.csv")

ejercicio_1 = df.shape
ejercicio_2 = len(df["Games"].unique())
ejercicio_3 = df["Season"].value_counts('%')
ejercicio_4 = df[(df["Year"] == (df[df["Season"] == "Summer"]["Year"].min())) & (df["Season"] == "Summer")]["City"].unique()
ejercicio_5 = df[(df["Year"] == (df[df["Season"] == "Winter"]["Year"].min())) & (df["Season"] == "Winter")]["City"].unique()
ejercicio_6 = df["NOC"].value_counts().head(10)
ejercicio_7 = df["Medal"].value_counts('%')
ejercicio_8 = df[(df["Year"] == (df[df["Season"] == "Summer"]["Year"].min())) & (df["Season"] == "Summer")]["NOC"].unique()

print(ejercicio_1)
print(ejercicio_2)
print(ejercicio_3)
print(ejercicio_4)
print(ejercicio_5)
print(ejercicio_6)
print(ejercicio_7)
print(ejercicio_8)
