import pandas as pd
import numpy as np

df = pd.read_csv("athlete_events.csv")

print(df.columns)
#print(df["Medal"].unique())
#print(df["Sex"].unique())

df_gold = df[df["Medal"] == "Gold"]
df_silver = df[df["Medal"] == "Silver"]
df_bronze = df[df["Medal"] == "Bronze"]
df_nomedal = df[df["Medal"].isnull()]

df_gold["Female"] = np.where(df_gold["Sex"] == 'F', 1, 0)
df_silver["Female"] = np.where(df_silver["Sex"] == 'F', 1, 0)
df_bronze["Female"] = np.where(df_bronze["Sex"] == 'F', 1, 0)
df_nomedal["Female"] = np.where(df_nomedal["Sex"] == 'F', 1, 0)

"""
print(df_gold["NOC"].value_counts().head(10))
print(df_silver["NOC"].value_counts().head(10))
print(df_bronze["NOC"].value_counts().head(10))
print(df_nomedal["NOC"].value_counts().head(10))

print(df_gold["Sex"].value_counts())
print(df_silver["Sex"].value_counts())
print(df_bronze["Sex"].value_counts())
print(df_nomedal["Sex"].value_counts())
"""
#def media_gender(df, analyze, gender = 'Female'):

df_new = df_gold.loc[:,["NOC","Female"]]
