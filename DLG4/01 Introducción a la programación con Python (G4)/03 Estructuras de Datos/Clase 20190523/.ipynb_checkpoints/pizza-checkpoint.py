def mostrar_ingredientes(lista, mensaje = "\tLos ingredientes son:\n"):
    lista.sort()
    c = 1
    r = ""
    r+= mensaje
    for i in lista:
        r+="\t"+str(c)+". "+i+"\n"
        c+=1
    return r

def buscar_en_ingrediente(lista, ingrediente):
    r = []
    c = 0
    for i in lista:
        if ingrediente.upper() in i.upper():
            r.append(i)
    return r

def cambiar_ingrediente(lista, ing1, ing2):
    #reemplaza en la lista el ingrediente ing1 por ing2
    for i in lista:
        if i == ing1:
            lista.remove(ing1)
    lista.append(ing2)
    return lista

pedido = ["masa tradicional", "salsa de tomate", "queso"]
masa = ["masa tradicional", "masa delgada"]
salsa = ["salsa de tomate", "salsa barbecue", "salsa de ajo"]
ingredientes = ["tomate", "aceitunas", "queso", "peperoni", "pollo"]
menu = "¡Gracias por ordenar con nosotros! ¿Qué desea realizar?\n1. Consultar ingredientes de la pizza\n2. Cambiar tipo de masa\n3. Cambiar tipo de salsa\n4. Agregar ingredientes\n5. Eliminar ingrediente\n6. Ordenar"
op = 0
tipo_masa = []

while op<6:
    print(menu)
    op = int(input())
    if op == 1:
        #Consultar ingredientes
        print(mostrar_ingredientes(pedido))
    elif op == 2:
        #Cambiar tipo de masa
        #Valido que haya masa en los ingredientes
        tipo_ing = buscar_en_ingrediente(pedido, 'masa')
        op_ing=0
        while op_ing==0:
            print(mostrar_ingredientes(masa, "\tElija tipo de masa:\n"))
            op_ing = int(input())
            if op_ing<=len(masa) and op_ing>0:
                if len(tipo_ing)==0:
                    #Se agrega un tipo de masa
                    pedido.append(masa[op_ing-1])
                else:
                    #Se cambia de tipo de masa
                    for i in tipo_ing:
                        pedido = cambiar_ingrediente(pedido, i, masa[op_ing-1])
            else:
                op_ing = 0
    elif op == 3:
        #Cambiar tipo de salsa
        #Valido que haya salsa en los ingredientes
        tipo_ing = buscar_en_ingrediente(pedido, 'salsa')
        op_ing=0
        while op_ing==0:
            print(mostrar_ingredientes(salsa, "\tElija tipo de salsa:\n"))
            op_ing = int(input())
            if op_ing<=len(salsa) and op_ing>0:
                if len(tipo_ing)==0:
                    #Se agrega un tipo de masa
                    pedido.append(salsa[op_ing-1])
                else:
                    #Se cambia de tipo de masa
                    for i in tipo_ing:
                        pedido = cambiar_ingrediente(pedido, i, salsa[op_ing-1])
            else:
                op_ing = 0
    elif op == 4:
        #Agregar ingrediente
        op_ing=0
        while op_ing==0:
            print(mostrar_ingredientes(ingredientes, "\tElige un ingrediente\n"))
            op_ing = int(input())
            if op_ing<=len(ingredientes) and op_ing>0:
                pedido.append(ingredientes[op_ing-1])
            else:
                op_ing=0
    elif op == 5:
        #Eliminar ingrediente
        op_ing=0
        while op_ing==0:
            print(mostrar_ingredientes(pedido, "\tElige un ingrediente para borrarlo\n"))
            op_ing = int(input())
            if op_ing<=len(pedido) and op_ing>0:
                pedido.remove(pedido[op_ing-1])
            else:
                op_ing=0
    else:
        print(mostrar_ingredientes(pedido, "Gracias!\nSu pizza tiene:\n"))
