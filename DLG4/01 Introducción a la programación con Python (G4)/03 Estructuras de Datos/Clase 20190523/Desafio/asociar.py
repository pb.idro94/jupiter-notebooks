import velocidad as v1
velocidad = [4, 4, 7, 7, 8, 9, 10, 10, 10,
             11, 11, 12, 12, 12, 12, 13, 13,
             13, 13, 14, 14, 14, 14, 15, 15,
             15, 16, 16, 17, 17, 17, 18, 18,
             18, 18, 19, 19, 19, 20, 20, 20,
             20, 20, 22, 23, 24, 24, 24, 24, 25]

distancia = [2, 10, 4, 22, 16, 10, 18, 26, 34,
             17, 28, 14, 20, 24, 28, 26, 34, 34,
             46, 26, 36, 60, 80, 20, 26, 54, 32,
             40, 32, 40, 50, 42, 56, 76, 84, 36,
             46, 68, 32, 48, 52, 56, 64, 66, 54,
             70, 92, 93, 120, 85]

prom_vel = v1.promedio(velocidad)
prom_dis = v1.promedio(distancia)

aux1=0
aux2=0
aux3=0
aux4=0

for i, x in enumerate(zip(velocidad, distancia)):
    if x[0]<prom_vel:
        aux1+=1
    if x[0]<prom_vel and x[1]>prom_dis:
        aux2+=1
    if x[0]>prom_vel:
        aux3+=1
    if x[0]>prom_vel and x[1]<prom_dis:
        aux4+=1

#print("Velocidad bajo el promedio: ",aux1)
#print("Velocidad bajo el promedio y distancia sobre el promedio: ",aux2)
#print("Velocidad sobre el promedio: ",aux3)
#print("Velocidad sobre el promedio y distancia bajo el promedio: ",aux4)
print(aux1)
print(aux2)
print(aux3)
print(aux4)
