import velocidad as v1
import listas_uno as l1

autos = l1.lista_anidada()

col1 = []

for i in autos:
    for j,x in enumerate(i):
        if j==1:
            col1.append(x)

prom = v1.promedio(col1)

print(list(filter(lambda x: x>prom, col1)))
