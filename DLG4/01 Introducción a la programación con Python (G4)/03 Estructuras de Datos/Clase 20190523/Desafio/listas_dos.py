import velocidad as v1
import listas_uno as l1

autos = l1.lista_anidada()

col1 = []
col2 = []
col4 = []

for i in autos:
    for j,x in enumerate(i):
        if j==1:
            col1.append(x)
        elif j==2:
            col2.append(x)
        elif j==4:
            col4.append(x)

print("Promedio: ",v1.promedio(col1))
print("Promedio: ",v1.promedio(col2))
print("Promedio: ",v1.promedio(col4))
