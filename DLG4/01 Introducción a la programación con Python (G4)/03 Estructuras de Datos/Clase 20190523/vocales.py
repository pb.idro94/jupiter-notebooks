def no_vowels(string):
    vocales = ['a','e','i','o','u']
    sin_vocales = [i for i in string if i not in vocales]
    return ''.join(sin_vocales)

print(no_vowels("murcielago"))
