create table inventario(
        codigo_producto integer,
        producto varchar(30),
        "local" varchar(30),
        precio integer,
        existencia varchar(30),
        stock integer,
        ubicacion varchar(50),
        numero_bodega integer,
        vendedor varchar(255),
        rut_vendedor bigint,
        numero_boleta integer,
        cantidad_vendida integer,
        rut_cliente bigint,
        nombre_cliente varchar(255)
);

copy inventario
FROM '/Users/hugopriest/Datacience 2019/Desafio Latam - Datascience/04 SQL para Data Science/02 Modelamiento y normalización/Desafio 2/Apoyo desafío.csv'
delimiter ',' csv header;

select * from inventario;

--1FN
-- Hay dos registros para leche y cafe los que fueron comprados por distintas personas
-- Se debe separar lo que es:
-- Producto (#codigo_producto, producto, precio)
-- Bodega (#numero_bodega, codigo_producto, existencia, stock, ubicacion)
-- Boleta (#numero_boleta, local, vendedor, rut_vendedor, cantidad_vendida, rut_cliente, nombre_cliente)

--2FN
-- Producto (#codigo_producto, producto, precio)
-- Bodega (#numero_bodega, codigo_producto, existencia, stock, ubicacion)
-- Vendedor (#rut_vendedor, vendedor)
-- Cliente (#rut_cliente, nombre_cliente)
-- Boleta (#numero_boleta, local, rut_vendedor, rut_cliente, #codigo_producto, cantidad_vendida)

--3FN
-- Producto (#codigo_producto, producto, precio)
-- Bodega (#numero_bodega, ubicacion)
-- Local (#local)
-- Local_Inventario (#local, #numero_bodega, #codigo_producto, existencia, stock)
-- Vendedor (#rut_vendedor, vendedor)
-- Cliente (#rut_cliente, nombre_cliente)
-- Boleta (#numero_boleta, #local, #rut_vendedor, #rut_cliente, #codigo_producto, cantidad_vendida)

--Creo las tablas
-- Producto (#codigo_producto, producto, precio)
create table producto(
        codigo_producto integer,
        producto varchar(50),
        precio integer,
        primary key (codigo_producto)
);

-- Bodega (#numero_bodega, ubicacion)
create table bodega(
        numero_bodega integer,
        ubicacion varchar(255),
        primary key (numero_bodega)
);

-- Local (#local)
create table local(
        "local" varchar(100),
        primary key ("local")
);

-- Vendedor (#rut_vendedor, vendedor)
create table vendedor(
        rut_vendedor integer,
        vendedor varchar(100),
        primary key (rut_vendedor)
);

-- Cliente (#rut_cliente, nombre_cliente)
create table cliente(
        rut_cliente integer,
        nombre_cliente varchar(100),
        primary key (rut_cliente)
);
-- Inventario (#local, #numero_bodega, #codigo_producto, existencia, stock)
create table local_inventario(
        "local" varchar(100),
        numero_bodega integer,
        codigo_producto integer,
        existencia boolean,
        stock integer,
        primary key ("local", numero_bodega, codigo_producto),
        foreign key ("local") references "local"("local"),
        foreign key (numero_bodega) references bodega(numero_bodega),
        foreign key (codigo_producto) references producto(codigo_producto)
);
         
-- Boleta (#numero_boleta, #local, #rut_vendedor, #rut_cliente, #codigo_producto, cantidad_vendida)
create table boleta(
        numero_boleta integer,
        "local" varchar(100),
        rut_vendedor integer,
        rut_cliente integer,
        codigo_producto integer,
        cantidad_vendida integer,
        primary key (numero_boleta, "local", rut_vendedor, rut_cliente, codigo_producto),
        foreign key ("local") references "local"("local"),
        foreign key (rut_vendedor) references vendedor(rut_vendedor),
        foreign key (rut_cliente) references cliente(rut_cliente),
        foreign key (codigo_producto) references producto(codigo_producto)
);

--Ahora cargo los datos
-- Producto (#codigo_producto, producto, precio)
insert into producto
select distinct codigo_producto, producto, precio
from inventario
order by codigo_producto;

-- Bodega (#numero_bodega, ubicacion)
insert into bodega
select distinct numero_bodega, ubicacion
from inventario
order by 1;

-- Local (#local)
insert into "local" 
select distinct "local"
from inventario;

-- Local_Inventario (#local, #numero_bodega, #codigo_producto, existencia, stock)
insert into local_inventario
select  distinct "local", 
        numero_bodega, 
        codigo_producto, 
        case when existencia = 'TRUE' or existencia = '1' or existencia = 'Si' then TRUE else FALSE end as existencia, 
        stock
from inventario
order by 2;

-- Vendedor (#rut_vendedor, vendedor)
insert into vendedor
select distinct rut_vendedor, vendedor
from inventario;

-- Cliente (#rut_cliente, nombre_cliente)
insert into cliente
select distinct rut_cliente, nombre_cliente
from inventario;

-- Boleta (#numero_boleta, #local, #rut_vendedor, #rut_cliente, #codigo_producto, cantidad_vendida)
insert into boleta 
select distinct numero_boleta, "local", rut_vendedor, rut_cliente, codigo_producto, cantidad_vendida
from inventario;

-- Pruebo con una consulta
select a.*, b.*, c.*, d.*
from boleta as a
join vendedor b on (a.rut_vendedor = b.rut_vendedor)
join cliente c on (a.rut_cliente = c.rut_cliente)
join producto d on (a.codigo_producto = d.codigo_producto);