--La canción que es el track número 4, 
--del primer artista que aparece en la querie que indica 
--los artistas de nacionalidad estadounidense que nacieron despues de 1992.


select c.*
from cancion c
where numero_del_track = 4
and artista in (
        select nombre_artista
        from artista a
        where int8(to_char(a.fecha_de_nacimiento, 'yyyy')) > 1992
        and a.nacionalidad = 'Estadounidense'
        limit 1);

