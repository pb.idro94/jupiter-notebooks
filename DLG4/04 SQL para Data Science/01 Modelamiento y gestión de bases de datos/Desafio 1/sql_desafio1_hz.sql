--1: creo la base de datos
create database biblioteca;

--2: creo la tabla libro
create table libro(
        id_libro bigint,
        nombre_libro varchar(255),
        autor varchar(255),
        genero varchar(255),
        primary key (id_libro)
);

--3 y 4: ingreso los datos de libro
insert into libro values (1, 'Sapo y Sepo', 'Josue Fredes', 'Terror');
insert into libro values (2, 'La Metamorfosis', 'Franz Kafka', 'Fantástica');

--5: creo la tabla prestamo
create table prestamo(
        id_prestamo bigint,
        id_libro bigint,
        nombre_persona varchar(255),
        fecha_inicio date,
        fecha_termino date,
        primary key (id_prestamo),
        foreign key (id_libro) references libro(id_libro)
);

--6: añado la col prestado en la tabla libro
alter table libro add prestado boolean default false;

--7: Ingresar el estado de prestamo de Sapo y Sepo
update libro
set prestado = true
where id_libro = 1;

--8. Ingresar el estado de prestamo de La Metamorfosis
update libro
set prestado = true
where id_libro = 2;

--9. Ingrese 5 prestamos asociados a Sapo y Sepo
insert into prestamo values (1, 1, 'Hugo Zúñiga', date('2019-01-01'), date('2019-02-10'));
insert into prestamo values (2, 1, 'Andrés Soto', date('2019-02-11'), date('2019-02-25'));
insert into prestamo values (3, 1, 'Favio Zúñiga', date('2019-02-26'), date('2019-03-02'));
insert into prestamo values (4, 1, 'Enrique Soto', date('2019-03-02'), date('2019-04-01'));
insert into prestamo values (5, 1, 'Deborah Stubing', date('2019-04-11'), date('2019-04-20'));

--10. Ingrese 6 prestamos asociados a La Metamorfosis
insert into prestamo values (6, 2, 'Hugo Zúñiga', date('2019-02-01'), date('2019-02-10'));
insert into prestamo values (7, 2, 'Andrés Soto', date('2019-03-11'), date('2019-03-25'));
insert into prestamo values (8, 2, 'Favio Zúñiga', date('2019-03-26'), date('2019-04-02'));
insert into prestamo values (9, 2, 'Enrique Soto', date('2019-04-02'), date('2019-05-01'));
insert into prestamo values (10, 2, 'Deborah Stubing', date('2019-05-11'), date('2019-05-20'));
insert into prestamo values (11, 2, 'Deborah Stubing', date('2019-06-11'), date('2019-06-20'));

--11. Cree un nuevo libro
insert into libro values (3, '1984', 'George Orwell', 'Ciencia Ficción');

--12. Seleccione los libros y las personas quienes lo pidieron prestado (nombre_libro y nombre_persona)
select l.nombre_libro, p.nombre_persona 
from libro as l
inner join prestamo as p on (l.id_libro = p.id_libro)
where l.id_libro = 1;

--13. Seleccione todas las columnas de la tabla Prestamo para los prestamos de Sapo y Sepo, ordenados decrecientemente por fecha_de_inicio
select *
from prestamo
where id_libro = 1
order by fecha_inicio desc;