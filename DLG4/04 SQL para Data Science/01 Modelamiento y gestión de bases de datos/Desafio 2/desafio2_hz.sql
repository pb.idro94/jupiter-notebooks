-- Ejercicio 1

--creo tabla album
create table album (
        titulo_album varchar(255),
        artista varchar(255),
        anio integer
);

--importo el archivo album.csv en la tabla album
copy album FROM '/Users/hugopriest/Datacience 2019/Desafio Latam - Datascience/04 SQL para Data Science/01 Modelamiento y gestión de bases de datos/Desafio 2/files/Album.csv' delimiter ',' csv header;

--reviso los datos
select * from album;

--creo tabla artista
create table artista(
        nombre_artista varchar(255),
        fecha_de_nacimiento date,
        nacionalidad varchar(255)
);

--importo el archivo artista.csv y lo cargo en la tabla artista
copy artista FROM '/Users/hugopriest/Datacience 2019/Desafio Latam - Datascience/04 SQL para Data Science/01 Modelamiento y gestión de bases de datos/Desafio 2/files/Artista.csv' delimiter ',' csv header encoding 'iso-8859-1';

--reviso los datos
select * from artista;

--creo tabla canción

create table cancion(
        titulo_cancion varchar(255),
        artista varchar(255),
        album varchar(255),
        numero_del_track integer
);

--importo el archivo cancion y lo cargo en la tabla cancion
copy cancion FROM '/Users/hugopriest/Datacience 2019/Desafio Latam - Datascience/04 SQL para Data Science/01 Modelamiento y gestión de bases de datos/Desafio 2/files/Cancion.csv' delimiter ',' csv header;

--reviso los datos
select * from cancion;

-- Ejercicio 2

--Canciones que salieron el año 2018

select a.titulo_album, a.artista, a.anio, b.titulo_cancion, b.numero_del_track
from album a
inner join cancion b on (a.titulo_album = b.album)
where a.anio = 2018;

--Albums y la nacionalidad de su artista

select a.titulo_album, b.nombre_artista, b.nacionalidad 
from album a
inner join artista b on (a.artista = b.nombre_artista);

--Número de track, cancion, album, año de lanzamiento y artista 
--donde las canciones deberán estar ordenadas por año de lanzamiento del albúm, album y artista correspondiente.

select c.numero_del_track, c.titulo_cancion, b.titulo_album, b.anio, b.artista
from album b
inner join cancion c on (b.titulo_album = c.album)
order by b.anio, b.titulo_album, b.artista;