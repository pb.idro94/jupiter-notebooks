# funciones Creadas por "Alejandra" en desafío anterior

def var_per(df, var, print_list=False):
    total_missing = df[var].isnull().sum()
    total_cells = len(df[var])
    porcentaje = (total_missing*100)/total_cells

    if print_list == True:
        print(df[df[var].isnull()== True]['cname'])

    return total_missing, porcentaje
