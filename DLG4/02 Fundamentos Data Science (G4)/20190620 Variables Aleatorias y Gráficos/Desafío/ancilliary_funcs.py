def obs_perdidas(df, var, print_list=False):
    p1 = df[var].isnull().sum()
    p2 = df[var].isnull().sum()/len(df)
    if print_list is True:
        p3 = df[df[var].isnull()==True]['cname']
        print(p3)
    return p1,p2
