variables = {
    'sbp':'Presión Sanguínea Sistólica',
    'tobacco':'Promedio tabaco consumido por día',
    'ldl':'Lipoproteína de baja densidad',
    'adiposity':'Adiposidad',
    'famhist':'Antecedentes familiares de enfermedades cardiácas (Binaria)',
    'types':'Personalidad tipo A',
    'obesity':'Obesidad',
    'alcohol':'Consumo actual de alcohol',
    'age':'Edad',
    'chd':'Enfermedad coronaria (dummy)'}
