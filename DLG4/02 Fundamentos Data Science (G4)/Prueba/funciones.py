import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Valores perdidos
def clean_data(df, lost_values='?'):
    """
    Reemplaza un string por nan de numpy.

    Parameters
    ----------
    df: DataFrame al que se le quiere reemplazar el lost_values por nan.
    lost_values: Valor que será reemplazado.

    Returns
    -------
    DataFrame
    """
    return df.replace(lost_values, np.nan)

def inspeccion_variable(df,var):
    """
    Grafica una variable de un DataFrame.
    En caso de ser una variable binaria, se muestra un gráfico de barras.
    En caso de ser una varibale continua, se muestra la distribución.

    Parameters
    ----------
    df: DataFrame
    var: Recibe una variable para ser graficada

    Returns
    -------
    Muestra un gráfico de barras o un gráfico con la distribución de la variable.

    """
    plt.figure()
    if len(df[var].value_counts())>2:
        sns.distplot((df[var]))
    else:
        sns.countplot(df[var])
    plt.title(var)
    plt.tight_layout()
    plt.show()

def graf_var_categorica(df,var):
    """
    Grafico de frecuencias de los valores de una variable

    Parameters
    ----------
    df: DataFrame
    var: Variable a graficar

    Returns
    -------
    Muestra un gráfico de barra con las frecuencias
    """
    serie = df[var].value_counts('%')
    new = pd.DataFrame({'val':serie.index, 'cant':serie.values})
    plt.figure()
    plt.bar(new['val'],new['cant'])
    plt.xticks(rotation=45)
    plt.title(var)
    plt.show()

def get_significance_variables(m, p=0.05):
    """
    Entrega las variables con significancia estadística según el valor indicado

    Parameters
    ----------
    m: Modelo
    p: Nivel de significancia (por defecto 0.05)

    Returns
    -------
    Serie con las variables significativas
    """
    r = m.summary2().tables[1]
    r = r[r[r.columns[3]] < p]
    return pd.Series(r.index)

def predict_pr(m, v):
    """
    Retorna la probabilidad de ocurrencia según el vector con valores de las variables

    Parameters
    ----------
    m: Modelo
    v: Vector a evaluar (debe contener las variables evaluadas en el modelo)

    Returns
    -------
    Retorna la probabilidad de ocurrencia según los parámetros del vector
    """
    y_pr = m.params['Intercept']
    for i,j in v.iteritems():
        y_pr += m.params[i] * j
    P = 1 / (1 + np.exp(-y_pr))
    return P
