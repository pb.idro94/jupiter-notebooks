
variables = {
'crim':'Tasa de criminalidad por sector de Boston',
'zn':'Proporción de terreno residencial asignado para terrenos baldíos',
'indus':'Proporción de negocios no asociados al comercio por sector',
'chas':'Dummy. 1 si el sector colinda con el río Charlos, 0 de lo contrario',
'nox':'Concentración de dioxido de carbono',
'rm':'Cantidad promedio de habitaciones por casa',
'age':'Proporsión de casas construidas antes de 1940',
'dis':'Distancia promedio a cinco centros de empleos',
'rad':'Índice de accesibilidad a autopistas',
'tax':'Nivel de impuestos asociados a viciendas',
'ptratio':'Razón alumno:profesor por sector de Boston',
'black':'Proporsión de afroamericanos por sector de Boston',
'lstat':'Porcentaje de población de estratos bajos',
'medv':'Valor mediano de las casas'}
