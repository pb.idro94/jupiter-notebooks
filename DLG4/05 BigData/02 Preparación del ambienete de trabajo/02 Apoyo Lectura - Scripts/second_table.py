#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import random, csv
from faker import Faker

get_faker = Faker()

def create_random_row(i):
    population = np.random.poisson(1000000)
    restaurants_number = np.random.negative_binomial(50, .7)
    common_demographics_race = np.random.choice(['Caucasian', 'Afroamerican', 'Asian', 'Indian', 'Hispanic'], 1, )[0]
    common_demographics_age = np.random.normal(45, 10.2,size=1)[0]
    common_demographics_class = np.random.choice(['A', 'B', 'C', 'D', 'E'], 1)[0]

    return i, population, restaurants_number, common_demographics_age, common_demographics_race, common_demographics_class

with open('zone_data.csv', 'w') as csvfile:
    file = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for i in ['I', 'II', 'III', 'IV', 'V','VI', 'VII', 'VIII']:
        file.writerow(create_random_row(i))









