import random
import csv

def create_random_row():
    age = random.randint(18, 90)
    income = random.randrange(10000, 1000000, step=1000)
    employment_status = random.choice(['Unemployed', 'Employed'])
    debt_status = random.choice(['Debt', 'No Debt'])
    churn_status = random.choice(['Churn', 'No Churn'])

    return [age, income, employment_status, use_in_minutes, debt_status, churn_status]

with open('simulate_purchases.csv', w) as csvfile:
    file = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for _ in range(10000):
        file.writerow(create_random_row())
        
print("Script Listo!")