#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

feed_mapper_output = sys.stdin

previous_counter = None
total_word_count = 0

for line_ocurrence in feed_mapper_output:
    word, ocurrence = line_ocurrence.split('\t')

    if word is not previous_counter:
        print(f"{str(total_word_count)}\t{previous_counter}")
    previous_counter = word
