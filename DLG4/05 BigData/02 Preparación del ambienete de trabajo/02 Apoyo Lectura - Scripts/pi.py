from random import random
from operator import add

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("Calculando pi").getOrCreate()

partitions = 1000
n = 100000 * partitions


def f(_):
    x = random() * 2 - 1
    y = random() * 2 - 1
    return 1 if x ** 2 + y ** 2 <= 1 else 0


count = spark.sparkContext.parallelize(range(1, n + 1), partitions).map(f).reduce(add)

f = open("calculo_pi.txt", "w")
f.write("Pi es aproximadamente %f\n" % (4 * count / n))
f.close()
spark.stop()
