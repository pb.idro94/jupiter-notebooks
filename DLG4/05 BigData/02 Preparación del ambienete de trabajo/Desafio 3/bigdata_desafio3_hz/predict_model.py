# importo las librerias clasicas
import numpy as np
import pandas as pd

# importo librerias para procesamiento
import pickle

# genero funcion para recodificar los datos
def prepara_data(df, train_data=True):

    # recodifico la suscripcion
    df['bin_subscription_Monthly'] = np.where(df.subscription_type == 'Monthly', 1, 0)
    df['bin_subscription_Yearly'] = np.where(df.subscription_type == 'Yearly', 1, 0)
    df['bin_subscription_Prepaid'] = np.where(df.subscription_type == 'Prepaid', 1, 0)
    df['bin_subscription_Semestral'] = np.where(df.subscription_type == 'Semestral', 1, 0)
    df['bin_subscription_Free'] = np.where(df.subscription_type == 'Free', 1, 0)
    df = df.drop(columns=['subscription_type'])

    # recodifico la zona
    df['bin_zone_I'] = np.where(df.delivery_zone == 'I', 1, 0)
    df['bin_zone_II'] = np.where(df.delivery_zone == 'II', 1, 0)
    df['bin_zone_III'] = np.where(df.delivery_zone == 'III', 1, 0)
    df['bin_zone_IV'] = np.where(df.delivery_zone == 'IV', 1, 0)
    df['bin_zone_V'] = np.where(df.delivery_zone == 'V', 1, 0)
    df['bin_zone_VI'] = np.where(df.delivery_zone == 'VI', 1, 0)
    df['bin_zone_VII'] = np.where(df.delivery_zone == 'VII', 1, 0)
    df = df.drop(columns=['delivery_zone'])

    # recodifico menu
    df['bin_menu_Japanese'] = np.where(df.menu == 'Japanese', 1, 0)
    df['bin_menu_Mexican'] = np.where(df.menu == 'Mexican', 1, 0)
    df['bin_menu_Indian'] = np.where(df.menu == 'Indian', 1, 0)
    df['bin_menu_French'] = np.where(df.menu == 'French', 1, 0)
    df['bin_menu_Italian'] = np.where(df.menu == 'Italian', 1, 0)
    df = df.drop(columns=['menu'])

    if train_data:
        # genero el vector objetivo
        delay_mean = df['delay_time'].mean()
        df['bin_delay'] = np.where(df['delay_time'] > delay_mean, 1, 0)
        df = df.drop(columns=['delay_time'])

    return df

# leo la data de prueba
tmp_df = pd.read_csv('test_delivery_data.csv', header=None)
tmp_df.columns = ['deliverer_id', 'delivery_zone', 'subscription_type', 'monthly_app_usage', 'customer_size', 'paid_price', 'menu']

# aplico la funcion para preparar la data
new_df = prepara_data(tmp_df, False)

# leo la data de prueba
tmp_df = pd.read_csv('test_delivery_data.csv', header=None)
tmp_df.columns = ['deliverer_id', 'delivery_zone', 'subscription_type', 'monthly_app_usage', 'customer_size', 'paid_price', 'menu']
# importo el modelo serializado
best_model = pickle.load(open("desafio3_best_model.pkl", "rb"))
# hago las predicciones
predictions = best_model.predict(new_df)
# las agrego el dataframe
tmp_df['delay_prediction'] = predictions

# para la probabilidad por zona
# cuento las apariciones por clase
prob_total = pd.DataFrame(tmp_df['delivery_zone'].value_counts())
prob_total.columns = ['total']
# cuento las apariciones de los pedidos con retraso
prob_delay = pd.DataFrame(tmp_df[tmp_df['delay_prediction'] == 1]['delivery_zone'].value_counts())
prob_delay.columns = ['delay']
# uno la información
prob_result = pd.concat([prob_total, prob_delay], axis=1, join='inner')
# calculo la probabilidad
prob_result['prob_delay'] = prob_result['delay'] / prob_result['total']

# genero un archivo con las probabilidades
file = open('eval_pr.txt','w')
file.write('La probabilidad que un pedido se atrase por sobre la media, para cada una de las zonas de envio:\n\n')
file.writelines(prob_result['prob_delay'].to_string())
file.close()

# para la probabilidad por repartidores
# cuento las apariciones por clase
prob_total = pd.DataFrame(tmp_df['deliverer_id'].value_counts())
prob_total.columns = ['total']
# cuento las apariciones de los pedidos con retraso
prob_delay = pd.DataFrame(tmp_df[tmp_df['delay_prediction'] == 1]['deliverer_id'].value_counts())
prob_delay.columns = ['delay']
# uno la información
prob_result = pd.concat([prob_total, prob_delay], axis=1, join='inner')
# calculo la probabilidad
prob_result['prob_delay'] = prob_result['delay'] / prob_result['total']

# genero un archivo con las probabilidades
file = open('eval_pr.txt','a')
file.write('\n\nLa probabilidad que un pedido se atrase por sobre la media, para cada uno de los repartidores:\n\n')
file.writelines(prob_result['prob_delay'].to_string())
file.close()

# para la probabilidad por menu
# cuento las apariciones por clase
prob_total = pd.DataFrame(tmp_df['menu'].value_counts())
prob_total.columns = ['total']
# cuento las apariciones de los pedidos con retraso
prob_delay = pd.DataFrame(tmp_df[tmp_df['delay_prediction'] == 1]['menu'].value_counts())
prob_delay.columns = ['delay']
# uno la información
prob_result = pd.concat([prob_total, prob_delay], axis=1, join='inner')
# calculo la probabilidad
prob_result['prob_delay'] = prob_result['delay'] / prob_result['total']

# genero un archivo con las probabilidades
file = open('eval_pr.txt','a')
file.write('\n\nLa probabilidad que un pedido se atrase por sobre la media, para cada uno de los menus:\n\n')
file.writelines(prob_result['prob_delay'].to_string())
file.close()

# para la probabilidad por subscription
# cuento las apariciones por clase
prob_total = pd.DataFrame(tmp_df['subscription_type'].value_counts())
prob_total.columns = ['total']
# cuento las apariciones de los pedidos con retraso
prob_delay = pd.DataFrame(tmp_df[tmp_df['delay_prediction'] == 1]['subscription_type'].value_counts())
prob_delay.columns = ['delay']
# uno la información
prob_result = pd.concat([prob_total, prob_delay], axis=1, join='inner')
# calculo la probabilidad
prob_result['prob_delay'] = prob_result['delay'] / prob_result['total']

# genero un archivo con las probabilidades
file = open('eval_pr.txt','a')
file.write('\n\nLa probabilidad que un pedido se atrase por sobre la media, para cada una de las subscripciones:\n\n')
file.writelines(prob_result['prob_delay'].to_string())
file.close()
