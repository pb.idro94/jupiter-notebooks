# importo las librerias clasicas
import numpy as np
import pandas as pd

# importo librerias para procesamiento
import pickle
from sklearn.model_selection import train_test_split

# modelos a entrenar
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import BernoulliNB

# reporte metricas de desempeño
from sklearn.metrics import classification_report

# importo archivo
df = pd.read_csv('train_delivery_data.csv', header=None)
df.columns = ['deliverer_id', 'delivery_zone', 'subscription_type', 'monthly_app_usage', 'customer_size', 'paid_price', 'menu', 'delay_time']

# genero funcion para recodificar los datos
def prepara_data(df, train_data=True):
    # recodifico la suscripcion
    df['bin_subscription_Monthly'] = np.where(df.subscription_type == 'Monthly', 1, 0)
    df['bin_subscription_Yearly'] = np.where(df.subscription_type == 'Yearly', 1, 0)
    df['bin_subscription_Prepaid'] = np.where(df.subscription_type == 'Prepaid', 1, 0)
    df['bin_subscription_Semestral'] = np.where(df.subscription_type == 'Semestral', 1, 0)
    df['bin_subscription_Free'] = np.where(df.subscription_type == 'Free', 1, 0)
    df = df.drop(columns=['subscription_type'])

    # recodifico la zona
    df['bin_zone_I'] = np.where(df.delivery_zone == 'I', 1, 0)
    df['bin_zone_II'] = np.where(df.delivery_zone == 'II', 1, 0)
    df['bin_zone_III'] = np.where(df.delivery_zone == 'III', 1, 0)
    df['bin_zone_IV'] = np.where(df.delivery_zone == 'IV', 1, 0)
    df['bin_zone_V'] = np.where(df.delivery_zone == 'V', 1, 0)
    df['bin_zone_VI'] = np.where(df.delivery_zone == 'VI', 1, 0)
    df['bin_zone_VII'] = np.where(df.delivery_zone == 'VII', 1, 0)
    df = df.drop(columns=['delivery_zone'])

    # recodifico menu
    df['bin_menu_Japanese'] = np.where(df.menu == 'Japanese', 1, 0)
    df['bin_menu_Mexican'] = np.where(df.menu == 'Mexican', 1, 0)
    df['bin_menu_Indian'] = np.where(df.menu == 'Indian', 1, 0)
    df['bin_menu_French'] = np.where(df.menu == 'French', 1, 0)
    df['bin_menu_Italian'] = np.where(df.menu == 'Italian', 1, 0)
    df = df.drop(columns=['menu'])

    if train_data:
        # genero el vector objetivo
        delay_mean = df['delay_time'].mean()
        df['bin_delay'] = np.where(df['delay_time'] > delay_mean, 1, 0)
        df = df.drop(columns=['delay_time'])

    return df

df = prepara_data(df)

# conjuntos de entrenamiento
X_train, X_test, y_train, y_test = train_test_split(df.drop(columns=['bin_delay']),
                                                    df['bin_delay'],
                                                    test_size=.33, random_state=11238)

# entreno modelos
#print('LogisticRegression:\n')
model_1 = LogisticRegression().fit(X_train, y_train)
yhat_1 = model_1.predict(X_test)
#print(classification_report(y_test, yhat_1))

#print('DecisionTreeClassifier:\n')
model_2 = DecisionTreeClassifier().fit(X_train, y_train)
yhat_2 = model_2.predict(X_test)
#print(classification_report(y_test, yhat_2))

#print('RandomForestClassifier:\n')
model_3 = RandomForestClassifier().fit(X_train, y_train)
yhat_3 = model_3.predict(X_test)
#print(classification_report(y_test, yhat_3))

#print('GradientBoostingClassifier:\n')
model_4 = GradientBoostingClassifier().fit(X_train, y_train)
yhat_4 = model_4.predict(X_test)
#print(classification_report(y_test, yhat_4))

#print('BernoulliNB:\n')
model_5 = BernoulliNB().fit(X_train, y_train)
yhat_5 = model_5.predict(X_test)
#print(classification_report(y_test, yhat_5))

# genero un archivo con el reporte de clasificacion
file = open('candidate_models.txt','w')
file.write('LogisticRegression:\n')
file.write(classification_report(y_test, yhat_1)+"\n")

file.write('DecisionTreeClassifier:\n')
file.write(classification_report(y_test, yhat_2)+"\n")

file.write('RandomForestClassifier:\n')
file.write(classification_report(y_test, yhat_3)+"\n")

file.write('GradientBoostingClassifier:\n')
file.write(classification_report(y_test, yhat_4)+"\n")

file.write('BernoulliNB:\n')
file.write(classification_report(y_test, yhat_5)+"\n")

file.close()

# serializo el mejor modelo y lo serializo
accuracy_df = pd.DataFrame([
    ('LogisticRegression', model_1, classification_report(y_test, yhat_1, output_dict=True)['accuracy']),
    ('DecisionTreeClassifier', model_2, classification_report(y_test, yhat_2, output_dict=True)['accuracy']),
    ('RandomForestClassifier', model_3, classification_report(y_test, yhat_3, output_dict=True)['accuracy']),
    ('GradientBoostingClassifier', model_4, classification_report(y_test, yhat_4, output_dict=True)['accuracy']),
    ('BernoulliNB', model_5, classification_report(y_test, yhat_5, output_dict=True)['accuracy'])
], columns=['model_name','model_obj', 'model_accuracy'])
max_accuracy = 0
best_model = ()
for _,j in accuracy_df.iterrows():
    if j[2] > max_accuracy:
        max_accuracy = j[2]
        best_model = j
pickle.dump(best_model[1], open('desafio3_best_model.pkl', 'wb'))
