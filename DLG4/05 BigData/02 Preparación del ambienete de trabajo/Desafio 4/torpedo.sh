wget -O clarissa_1.txt  http://www.gutenberg.org/cache/epub/9296/pg9296.txt
wget -O clarissa_2.txt  http://www.gutenberg.org/cache/epub/9798/pg9798.txt
wget -O clarissa_3.txt  http://www.gutenberg.org/cache/epub/9881/pg9881.txt
wget -O clarissa_4.txt  http://www.gutenberg.org/cache/epub/10462/pg10462.txt
wget -O clarissa_5.txt  http://www.gutenberg.org/cache/epub/10799/pg10799.txt
wget -O clarissa_6.txt  http://www.gutenberg.org/cache/epub/11364/pg11364.txt
wget -O clarissa_7.txt  http://www.gutenberg.org/cache/epub/11889/pg11889.txt
wget -O clarissa_8.txt  http://www.gutenberg.org/cache/epub/12180/pg12180.txt
wget -O clarissa_9.txt  http://www.gutenberg.org/cache/epub/12398/pg12398.txt

cat ./richardson_clarissa/clarissa_1.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_1.txt
cat ./richardson_clarissa/clarissa_2.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_2.txt
cat ./richardson_clarissa/clarissa_3.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_3.txt
cat ./richardson_clarissa/clarissa_4.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_4.txt
cat ./richardson_clarissa/clarissa_5.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_5.txt
cat ./richardson_clarissa/clarissa_6.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_6.txt
cat ./richardson_clarissa/clarissa_7.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_7.txt
cat ./richardson_clarissa/clarissa_8.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_8.txt
cat ./richardson_clarissa/clarissa_9.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_9.txt

cat ./richardson_clarissa/clarissa_9.txt | python mapper.py | python filter.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_9.txt

for i in {1..9}; do cat clarissa_${i}.txt | python mapper.py | sort -k1,1 | python reducer.py > word_count_clarissa_${i}.txt; done


ls | grep csv | while read filename; do echo $filename; done


s3://bigdata-desafio/challenges/u2act2/survey_data.txt

# para cada una de las líneas en los datos
# for line_in_document in feed_document:
#     convierto el string en una lista
#     str_list = list(line_in_document.split(','))
#     identifico el plan
#     plan = str(str_list[-1])[0]
#     votes = str_list[:50]
#     positive_votes = votes.count('1')
#     print("Plan = {}, Votes = {}".format(plan, votes))
#     print(plan + '\t' + str(positive_votes))
