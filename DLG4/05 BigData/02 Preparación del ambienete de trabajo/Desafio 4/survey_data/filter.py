#filter.py

# importamos la librería de sistema
import sys

# vamos a leer los datos que provengan del standard input del paso previo
feed_reducer_output = sys.stdin

# defino lista con las palabras a filtrar
word_filtered = ['clarissa', 'arabella', 'robert', 'james']

# para cada una de las líneas en el standard input del paso previo
for line_ocurrence in feed_reducer_output:
    #vamos a separar entre palabra e indicador
    #recordando que el standard input en esta etapa será # <palabra><tab><ocurrencia>
    word, ocurrence = line_ocurrence.split('\t')

    # Imprimo la linea si la palabra está dentro del listado
    if (word != '') & (word != ' ') & (word.lower() in word_filtered):
        print(str(word) + '\t' + str(ocurrence))
