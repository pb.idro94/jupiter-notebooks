#reducer.py

# importamos la librería de sistema
import sys

# importo pandas
import pandas as pd

# vamos a leer los datos que provengan del standard input del paso previo
feed_mapper_output = sys.stdin
#df = pd.Series(sys.stdin)

# generamos un identificador de la palabra previa
previous_counter = None

# generamos un contador de la cantidad de palabras
total_word_count = 0

# separamos la palabra de la ocurrencia
# df = df.str.split(',')

# tmp_df = pd.DataFrame()
# tmp_df['plan'] = df.map(lambda x: x[0])
# tmp_df['positive'] = df.map(lambda x: int(x[1]))
# del df
# print(tmp_df.groupby(['plan'])['positive'].agg('sum'))

# para cada una de las líneas en el standard input del paso previo
for line_ocurrence in feed_mapper_output:
    #vamos a separar entre palabra e indicador #recordando que el standard input en esta etapa será # <palabra> 1
    s = [ord(c) for c in line_ocurrence]
    if s != [10]:
        word, ocurrence = line_ocurrence.split(chr(44))
        if int(ocurrence) >= 30:
            # si es que la palabra es distinta a la previa # procedemos a contarla
            if word != previous_counter:
                # si la palabra no es la primera
                if previous_counter is not None:
                    # vamos a imprimir un resultado intermedio
                    print(previous_counter + chr(44) + str(total_word_count))

                # asignamos al previous counter la palabra nueva
                previous_counter = word
                # reseteamos el contador para la palabra
                total_word_count = 0
            # contamos la cantida de ocurrencias
            total_word_count += int(ocurrence)
# imprimimos el resultado final
print(previous_counter + chr(44) + str(total_word_count))
