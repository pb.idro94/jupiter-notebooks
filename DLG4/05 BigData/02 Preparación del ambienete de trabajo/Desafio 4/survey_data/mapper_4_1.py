#mapper.py

# vamos a importar las librerías de expresiones regulares y de sistema
import sys
import pandas as pd

# vamos a leer los datos que provengan del standard input del sistema
#feed_document = sys.stdin

# creo una Serie con la data
df = pd.Series(sys.stdin)

# separo por coma los datos
df = df.str.split(',')

# Cuento la cantidad de respuestas positivas y concateno el plan
plan = df.map(lambda x: x[50][0] + chr(44) + '1').to_string(index=False).strip().replace(chr(32),'')

print(plan)
