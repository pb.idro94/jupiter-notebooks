filename="${0##*/}"
echo $filename
ini=$(date +'%Y-%m-%d %H:%M:%S')
echo 'inicio: '${ini} 
head -n 100 survey_data.txt | python mapper_4_3.py | sort | python reducer_4_3.py > survey_data_4_3.txt;
#head -n 100 survey_data.txt | python mapper_4_3.py | sort > survey_data_4_4.txt;
end=$(date +'%Y-%m-%d %H:%M:%S')
echo 'fin:    '${end}
echo "================================================" >> $filename.log
echo $filename >> $filename.log
echo 'inicio: '${ini} >> $filename.log
echo 'fin:    '${end} >> $filename.log
echo "================================================" >> $filename.log