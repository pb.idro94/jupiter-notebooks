ini=$(date +'%Y-%m-%d %H:%M:%S')
for i in {1..9}; do cat ./clarissa_${i}.txt | python mapper.py | sort | python reducer.py > ./richardson_clarissa_wordcount/word_count_clarissa_${i}.txt; done
end=$(date +'%Y-%m-%d %H:%M:%S')
echo 'inicio: '${ini}
echo 'fin:    '${end}