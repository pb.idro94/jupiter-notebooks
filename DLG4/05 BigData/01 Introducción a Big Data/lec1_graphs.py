#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import sample, seed
from copy import copy
from time import time
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


def constant_complexity(n):
    """
    simulate constant complexity behavior
    """
    return 1

def linear_complexity(n):
    """
    simulate positive proportional behavior conditional to array
    """
    array = []

    for i in n:
        array.append(i)

    return len(array)

def logarithmic_complexity(n):
    """
    simulate logarithmic behavior conditional to array and a division/multiplication operation
    """
    array = []

    while len(n) > 1:
        array = array + [n[0: len(n) // 2]]
        n = n[0: len(n) // 2]

    return len(array)

def quadratic_complexity(n):
    """
    simulate power behavior conditional to array and a constant operation at the innermost.
    """
    array = []

    for _ in n:
        for j in n:
            array.append(j)

    return len(array)



def cubic_complexity(n):
    """
    simulate power behavior conditional to array and a constant operation at the innermost.
    """

    array = []

    for _ in n:
        for _ in n:
            for k in n:
                array.append(k)


    return len(array)


def insertion_sort(array):

    for current_element in range(2, len(array)):
        key = array[current_element]
        previous_element = current_element - 1
        while previous_element > 0 and key < array[previous_element]:
            array[previous_element + 1] = array[previous_element]
            previous_element = previous_element - 1

        array[previous_element + 1] = key


def bubble_sort(array):
    n = len(array)
    for i in range(n):
        for j in range(0, n - i -1):
            if array[j] > array[j + 1]:
                array[j], array[j + 1] = array[j + 1], array[j]


def compare_evaluations():
    seed(5)
    python_sorted, bubble_sorted, insertion_sorted = [], [], []
    evaluation_range = range(5000, 30000, 500)

    for i in evaluation_range:
        unordered_list = sample(range(100000), i)

        # python native sort
        start = time()
        sorted(copy(unordered_list))
        end = time()
        python_sorted.append(end - start)

        # bubble sort
        start = time()
        bubble_sort(copy(unordered_list))
        end = time()
        bubble_sorted.append(end - start)

        # insertion sort
        start = time()
        insertion_sort(copy(unordered_list))
        end = time()
        insertion_sorted.append(end - start)


    plt.figure(figsize=(10, 6))
    plt.plot(evaluation_range, python_sorted, '.-', label='Native Python Sorted')
    plt.plot(evaluation_range, bubble_sorted, '.-', label='Bubble Sorted')
    plt.plot(evaluation_range, insertion_sorted, '.-', label='Insertion Sorted')
    plt.xlabel('Cantidad de datos en la lista')
    plt.ylabel('Tiempo de ejecución (en segundos)')
    sns.despine()
    plt.legend(loc='center left', bbox_to_anchor=(1, .5))


def compare_algo_complexities(evaluation_range, log_values=False):
    const_comp, linear_comp, log_comp, quad_comp, cubic_comp = [],[],[],[],[]


    if log_values is True:
        funct = lambda x: np.log(x) + 0.001
    else:
        funct = lambda x: x

    for evaluation in evaluation_range:
        evaluate_array = list(range(evaluation))
        const_comp.append(constant_complexity(evaluate_array))
        linear_comp.append(linear_complexity(evaluate_array))
        log_comp.append(logarithmic_complexity(evaluate_array))
        quad_comp.append(quadratic_complexity(evaluate_array))
        cubic_comp.append(cubic_complexity(evaluate_array))


    plt.figure(figsize=(10, 6))
    plt.plot(evaluation_range, funct(const_comp), '.-', label="Constant Complexity")
    plt.plot(evaluation_range, funct(linear_comp) ,'.-', label="Linear Complexity" )
    plt.plot(evaluation_range, funct(log_comp) ,'.-', label="Logarithmic Complexity")
    plt.plot(evaluation_range, funct(quad_comp) ,'.-', label="Quadratic Complexity")
    plt.plot(evaluation_range, funct(cubic_comp) ,'.-', label="Cubic Complexity")

    if log_values is True:
        plt.ylabel('Cantidad de evaluaciones (en escala logarítmica)')
    else:
        plt.ylabel('Cantidad de evaluaciones')

    plt.xlabel('Dimensionado de la lista (1-D)')
    sns.despine()
    plt.legend(loc='center left', bbox_to_anchor=(1, .5))
    plt.show()





