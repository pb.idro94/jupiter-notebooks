select 
    country,
    sum(case when gender = '99999' then 1 else 0 end) as q_no_gender,
    sum(case when age = '99999' then 1 else 0 end) as q_no_age
from user_profile
group by country
order by q_no_gender+q_no_age desc;