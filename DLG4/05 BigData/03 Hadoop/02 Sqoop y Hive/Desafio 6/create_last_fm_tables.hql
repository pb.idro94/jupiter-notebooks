CREATE EXTERNAL TABLE user_profile(
    user_mboxsha1 VARCHAR(100),
    gender VARCHAR(5),
    age INT,
    country VARCHAR(100),
    signup_day_month VARCHAR(50),
    signup_year VARCHAR(50)
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';

CREATE EXTERNAL TABLE artists(
    user_mboxsha1 VARCHAR(100),
    musicbrainz_artist_id VARCHAR(100),
    artist_name VARCHAR(100),
    plays INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';