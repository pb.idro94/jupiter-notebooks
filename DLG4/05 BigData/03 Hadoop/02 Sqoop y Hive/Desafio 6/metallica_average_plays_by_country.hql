-- mayor reproducciones en promedio
select usr.country, avg(art.plays) as avg_plays
from artists as art
join user_profile as usr on (art.user_mboxsha1 = usr.user_mboxsha1)
where lower(art.artist_name) = 'metallica'
group by usr.country
order by avg_plays desc;

-- menor reproducciones en promedio
select usr.country, avg(art.plays) as avg_plays
from artists as art
join user_profile as usr on (art.user_mboxsha1 = usr.user_mboxsha1)
where lower(art.artist_name) = 'metallica'
group by usr.country
order by avg_plays asc;