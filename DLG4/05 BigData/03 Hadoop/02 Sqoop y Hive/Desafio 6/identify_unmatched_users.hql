select sum(1) as q_artist_unmatched
from user_profile as a
left outer join artists as b on (a.user_mboxsha1 = b.user_mboxsha1)
where b.musicbrainz_artist_id is null;