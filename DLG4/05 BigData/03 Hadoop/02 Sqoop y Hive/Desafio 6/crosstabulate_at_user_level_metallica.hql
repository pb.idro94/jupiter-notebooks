select usr.country, usr.gender, usr.age, sum(art.plays) as plays
from artists as art
join user_profile as usr on (art.user_mboxsha1 = usr.user_mboxsha1)
where lower(art.artist_name) = 'metallica'
group by usr.country, usr.gender, usr.age
order by plays desc;