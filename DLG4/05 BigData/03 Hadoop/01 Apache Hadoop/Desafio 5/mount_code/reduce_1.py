#!/usr/bin/python3.6

import sys

feed_mapper_output = sys.stdin
previous_counter = None
sum_relevance = 0.0
total_count = 0

for line in feed_mapper_output:
    (tag_id, relevance) = line.replace('\n','').split(',')

    if tag_id != previous_counter:
        if previous_counter is not None:
            print(f"{previous_counter},{str(sum_relevance/total_count)}")
        previous_counter = tag_id
        sum_relevance = 0.0
        total_count = 0
    
    sum_relevance += float(relevance)
    total_count += 1


print(f"{previous_counter},{str(sum_relevance / total_count)}")

#hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming-2.8.5-amzn-5.jar -file mapper_1.py -mapper mapper_1.py -file reduce_1.py -reducer mapper_1.py -input hdfs:///home/hadoop/movielens-20m/genome-scores.csv -output register-results
