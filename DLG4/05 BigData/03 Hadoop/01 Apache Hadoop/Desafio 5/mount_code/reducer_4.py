#!/usr/bin/python3.6

import sys

feed_mapper_output = sys.stdin
prev_cant_genres = None
sum_genres = 0

for line in feed_mapper_output:
    cant_genres = line.replace('\n','')

    if cant_genres != prev_cant_genres:
        if prev_cant_genres is not None:
            print(f"{str(prev_cant_genres)},{str(sum_genres)}")
        prev_cant_genres = cant_genres
        sum_genres = 0
    
    sum_genres += int(cant_genres)

print(f"{str(prev_cant_genres)},{str(sum_genres)}")

#hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming-2.8.5-amzn-5.jar \
# -file /mapper_4.py -mapper /mapper_4.py \
# -file /reducer_4.py -reducer /reducer_4.py \
# -input hdfs:///movielens-20m/movies.csv \
# -output results-4