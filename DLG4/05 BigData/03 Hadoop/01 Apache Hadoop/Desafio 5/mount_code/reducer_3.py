#!/usr/bin/python3.6

import sys

feed_mapper_output = sys.stdin
prev_movie = None
sum_ratings = 0.0
total_ratings = 0

for line in feed_mapper_output:
    (movie_id, rating) = line.replace('\n','').split(',')

    if movie_id != prev_movie:
        if prev_movie is not None:
            print(f"{prev_movie},{str(sum_ratings/total_ratings)}")
        prev_movie = movie_id
        sum_ratings = 0.0
        total_ratings = 0
    
    sum_ratings += float(rating)
    total_ratings += 1


print(f"{prev_movie},{str(sum_ratings / total_ratings)}")

#hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming-2.8.5-amzn-5.jar \
# -file /mapper_3.py -mapper /mapper_3.py \
# -file /reducer_3.py -reducer /reducer_3.py \
# -input hdfs:///movielens-20m/ratings.csv \
# -output results-3