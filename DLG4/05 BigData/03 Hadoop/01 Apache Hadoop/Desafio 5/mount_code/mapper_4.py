#!/usr/bin/python3.6

import re, sys

feed_document = sys.stdin

for line_in_document in feed_document:
    line = line_in_document.replace(', ','')
    comas = int(line.count(','))
    # no proceso las peliculas que tienen alguna coma en el nombre
    if comas == 2:
        (index, movie_name, genre) = line.replace('\n','').split(',')
        cant_genre = list(genre.replace('\n','').split('|'))
        print(f"{str(len(cant_genre))}")