#!/usr/bin/python3.6

import sys

feed_mapper_output = sys.stdin
prev_user = None
sum_ratings = 0.0
total_ratings = 0

for line in feed_mapper_output:
    (user_id, rating) = line.replace('\n','').split(',')

    if user_id != prev_user:
        if prev_user is not None:
            print(f"{prev_user},{str(total_ratings)},{str(sum_ratings/total_ratings)}")
        prev_user = user_id
        sum_ratings = 0.0
        total_ratings = 0
    
    sum_ratings += float(rating)
    total_ratings += 1


print(f"{prev_user},{str(total_ratings)},{str(sum_ratings / total_ratings)}")

#hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming-2.8.5-amzn-5.jar -file /mapper_2.py -mapper /mapper_2.py -file /reducer_2.py -reducer /reducer_2.py -input hdfs:///movielens-20m/ratings.csv -output results-2