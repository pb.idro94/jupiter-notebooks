#!/usr/bin/env python

from pyspark.sql import SparkSession
from pyspark.sql.types import StructField, StructType, StringType, FloatType, IntegerType
from pyspark.sql.functions import when, col, log
from pyspark.mllib.evaluation import RegressionMetrics
from pyspark.ml.recommendation import ALSModel, ALS



def get_dummies_pyspark(df, column, drop_first=True):
    """Port of pandas.get_dummies function

    :df: A pyspark.sql.dataframe.DataFrame object
    :column: Column to transform
    :drop_first: Whether to delete first category.
    :returns: An updated pyspark.sql.dataframe.DataFrame object with k-1 categories from column

    """
    # get available categories for column in DataFrame
    tmp_col_categories = df.select(column).distinct().rdd\
                            .flatMap(lambda x: x)\
                            .collect()

    # remove first category?
    if drop_first is True:
        tmp_col_categories = tmp_col_categories[1:]
    else:
        pass

    # generate CASE WHEN sql expression
    tmp_dummies_expression = [when(col(column) == 1, 1).otherwise(0)\
                                .alias(f"{column}_{str(i)}") for i in tmp_col_categories]
    
    # remove original column and concatenate results
    return df.select(df.drop(column).columns + tmp_dummies_expression)

def get_als_factors_information(model, n_users = 3, n_items = 10):
    """Report summary on factor loadings at item and user level

    :model: An pyspark.ml.recommendation.ALSModel object.
    :n_users: An integer. Quantity of users to report.
    :n_items: An integer. Quantity of items to report.
    :returns: A printed summary.

    """
    if isinstance(model, ALSModel) or isinstance(model, ALS):
        tmp_user_factors, tmp_item_factors = model.userFactors, model.itemFactors
        print(f"Número de usuarios en entrenamiento: {tmp_user_factors.count()}")
        print(f"Producto punto para los primeros {n_users} usuarios:")

        for i in tmp_user_factors.take(n_users):
            print(f"Usuario: {i.id} -> Producto punto: {[round(j, 3) for j in i.features]}" )

        print("\n")
        print(f"Número de items en entrenamiento: {tmp_item_factors.count()}")
        print(f"Producto punto para los primeros {n_items} items:")

        for i in tmp_item_factors.take(n_items):
            print(f"Item: {i.id} -> Producto punto: {[round(j, 3) for j in i.features]}")



def report_reg_metrics(metrics):
    """Report metrics from a RegressionMetrics object

    :metrics: a RegressionMetrics object
    :returns: a printed report.

    """

    if isinstance(metrics, RegressionMetrics) is True:
        print(f"Varianza Explicada: {round(metrics.explainedVariance, 3)}")
        print(f"Error cuadrático promedio: {round(metrics.meanSquaredError, 3)}")
        print(f"Error absoluto promedio: {round(metrics.meanAbsoluteError, 3)}")
        print(f"Raíz del error cuadrático promedio: {round(metrics.rootMeanSquaredError, 3)}")
    else:
        raise TypeError("metrics argument must be a pyspark.mllib.evaluation.RegressionMetrics object.")
