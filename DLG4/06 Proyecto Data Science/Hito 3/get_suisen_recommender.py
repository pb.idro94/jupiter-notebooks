import pandas as pd
import numpy as np

import warnings
warnings.filterwarnings('ignore')

import json

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from numpy import random
import pickle

# leemos la data de anime desde bucket s3
anime = pd.read_csv('s3://anime-recommender/anime_rep_covers.csv').drop(['Unnamed: 0'], axis=1)
df_anime = anime
df_ratings = pd.read_csv('s3://anime-recommender/rating.csv', sep=',')

distances2 = pickle.load(open('distances.sav', 'rb'))
indices2 = pickle.load(open('indices.sav', 'rb'))

def get_index_from_id(anime_id):
    return df_anime[df_anime.anime_id == anime_id].index.values[0]

def get_id_from_index(index):
    return df_anime[df_anime.index == index]['anime_id'].values[0]

def get_user_viewed_list(user):
    return list(df_ratings[df_ratings['user_id']==user]['anime_id'])

def get_user_top_list(user):
    df_user = df_ratings[df_ratings['user_id']==user]
    df_rated = df_user.dropna(how = 'any')
    avg =  df_rated.rating.mean() 
    df_toplist = df_rated[df_rated['rating']>= avg].sort_values('rating', ascending = False).head(10)
    return list(df_toplist['anime_id'])

def get_recommendations_ids(aid):
    anime =  get_index_from_id(aid)
    test = list(indices2[anime,1:11])
    nb = []
    for i in test:
        a_name = get_id_from_index(i)
        nb.append(a_name)
    return nb

def get_n_recommends_ids(user, n):
    vistas = list(get_user_viewed_list(user))
    liked = list(get_user_top_list(user))
    lista = []
    for i in liked:
        ani = pd.Series(get_recommendations_ids(i))
        recs = np.setdiff1d(ani, vistas) 
        lista.extend(recs)
        if(len(lista) > n):
            lista = lista[:n]
            break
    return lista

# genero función que envía el correo y marca la data como enviada
def send_email(data):
    """
    Función que envía correo con las respuestas.
    """
    
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    sender = "suisen.recommender@gmail.com"
    receiver = str(data['mail'].unique()[0])
    password = "suisensuisen"

    smtpserver = smtplib.SMTP("smtp.gmail.com",587)

    smtpserver.ehlo()

    smtpserver.starttls()

    smtpserver.ehlo

    smtpserver.login(sender,password)

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Suisen Recommender"
    msg['From'] = sender
    msg['To'] = receiver

    # Create the body of the message (a plain-text and an HTML version).
    text = "Hola!\nCómo estás?\nTe enviamos estas recomendaciones para que disfrutes del mejor animé!"
    body1 = """
    <html>
    <head></head>
    <body>
    <p>
    Hola!\nCómo estás?\nTe enviamos estas recomendaciones para que disfrutes del mejor animé!
    </p>
    <table>
    """
        
    body3 = """
    </table>
    </body>
    </html>
    """
    
    tmp_img = '<tr>'
    tmp_name = '<tr>'
    tmp_rating = '<tr>'
    
    for _, m in data.iterrows():
        tmp_img = tmp_img + '<td><img src="' + str(m[4]) + '" alt="'+ str(m[2]) +'" height="290" width="201"></td>'
        tmp_name = tmp_name + '<td>'+str(m[2])+'</td>'
        tmp_rating = tmp_rating + '<td>rating: '+str(m[3])+'</td>'
        
    tmp_img = tmp_img + '</tr>'
    tmp_name = tmp_name + '</tr>'
    tmp_rating = tmp_rating + '</tr>'
                                           
    html = body1 + tmp_img + tmp_name + tmp_rating + body3

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    smtpserver.sendmail(sender,receiver,msg.as_string())

    smtpserver.close()
    
# funcion que permite extraer la data desde las respuestas de google
def get_suisen_responses(modelo='ALS', sheet_name = 'Suisen_Responses',json_keyfile_name='suisen-recommender-da276c0e6744.json'):
    """
    Funcion que rescata las respuestas desde Google Drive
    """
    import pandas as pd
    import gspread
    from oauth2client.service_account import ServiceAccountCredentials

    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']

    creds = ServiceAccountCredentials.from_json_keyfile_name(json_keyfile_name, scope)

    client = gspread.authorize(creds)

    sheet = client.open(sheet_name).sheet1

    suisen_responses = sheet.get_all_records()

    tmp = []
    for i in suisen_responses:
        tmp_df = pd.DataFrame(i.values()).T
        tmp_df.columns = i.keys()
        tmp.append(tmp_df)
    
    # genero df con la data
    df = pd.concat(tmp).reset_index(drop=True)
    
    # filtro las respuestas enviadas
    df = df[df['Respuesta enviada'] == '']
    
    # genero los sin registro
    user_sin_reg = df[df['Ingresa tu SUISEN- ID : '] == '']
    
    # genero los con registro
    user_con_reg = df[df['Ingresa tu SUISEN- ID : '] != '']

    
    # PARA USUARIOS NO REGISTRADOS
    reco_list = []
    for i in user_sin_reg.iterrows():
        if (i[1][3] == 'Anime Series'):
            tipo = 'TV_type'
        elif i[1][3] == 'Anime Movies':
            tipo = 'Movie_type'
        filtrado = anime[anime[tipo] == 1]
        generos = i[1][2].replace(' ','').split(',')
        generos = [n + '_gen' for n in generos]
        filtrado['seleccionado'] = filtrado[generos].sum(axis=1)
        animes_elegidos = filtrado[filtrado['seleccionado'] > 0]
        animes_elegidos['RatioDos'] = (1/animes_elegidos['seleccionado']) * animes_elegidos['ratio']
        recomendaciones = animes_elegidos.sort_values(by = 'RatioDos', ascending = True).head(3)
        recomendaciones['mail'] = i[1][4]
        recomendaciones['fecha'] = i[1][0]
        recomendaciones = recomendaciones[['mail', 'fecha', 'name_anime', 'rating_avg', 'img']]
        reco_list.append(recomendaciones)
    
    # MANDAR CORREO Y MARCAR ENVIADA
    for mail in reco_list:
        # enviar correo
        send_email(mail)
    
    if modelo == 'ALS':
        # PARA USUARIOS REGISTRADOS
        rec = []
        rec_list = []
        for _, i in user_con_reg.iterrows():
            for line in open('./files/als_recomendaciones.json', 'r'):
                tmp = json.loads(line)
                if tmp['user_id'] == i[5]:
                    rec.append(tmp)
            for r in rec:
                tmp_rec = []
                for j in r['collect_set(anime_id)']:
                    tmp_rec.append(anime[anime['anime_id'] == j])
            recomendaciones = pd.concat(tmp_rec)
            recomendaciones['mail'] = i[4]
            recomendaciones['fecha'] = i[0]
            recomendaciones = recomendaciones[['mail', 'fecha', 'name_anime', 'rating_avg', 'img']]
            cant = 3
            if i[7].isnumeric():
                cant = i[7]
            rec_list.append(recomendaciones.head(cant))
            
    elif modelo == 'KNN':
        #resultado KNN
        rec_list = []
        knn_rec = []
        knn_tmp = []
        for _, i in user_con_reg.iterrows():
            cant = 3
            if i[7].isnumeric():
                cant = i[7]
            knn_tmp = get_n_recommends_ids(i[5],cant)
            for k in knn_tmp:
                knn_rec.append(anime[anime['anime_id'] == k])
            recomendaciones = pd.concat(knn_rec)
            recomendaciones['mail'] = i[4]
            recomendaciones['fecha'] = i[0]
            recomendaciones = recomendaciones[['mail', 'fecha', 'name_anime', 'rating_avg', 'img']]
            
            rec_list.append(recomendaciones.head(cant))
        
    # MANDAR CORREO 
    for mail2 in rec_list:
        # enviar correo
        send_email(mail2)
    
    # Marcar correos enviados
    for i in range(len(suisen_responses)):
        sheet.update_cell(i+2, 9, '1')

import sys

model = str(sys.argv)

get_suisen_responses(model)