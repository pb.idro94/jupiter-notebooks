# importamos el nuevo conjunto de dato
test_data = pd.read_csv('churn_test.csv').drop(columns='Unnamed: 0')
# implementamos la función de preprocesamiento de datos
test_data = preprocess_data(test_data)
# definimos el vector objetivo y la matriz de atributos
y = test_data.pop('churn_yes')
# generamos las PREDICCIONES DE PROBABILIDAD
tmp_pr = improve_gboost.best_estimator_.predict_proba(test_data)

# Generamos un nuevo conjunto de datos que contenga todas las variables binarias por estadi
predicted_proba = test_data.filter(regex='state_*', axis=1)
# extraemos la probabilidad de no fuga para cada registro
predicted_proba['pr_0'] = [i[0] for i in tmp_pr]
# extraemos la probabilidad de fuga para cada registro
predicted_proba['pr_1'] = [i[1] for i in tmp_pr]


# generamos tres listas vacios
p0, p1, state = [], [], []
# para cada estado ingresado en el nuevo conjunto de datos
for colname, serie in predicted_proba.loc[:, 'state_AL':'state_WY'].iteritems():
    # seleccionamos los registros existentes
    tmp_df = predicted_proba[serie == 1]
    # calculamos la media de no fuga y agregamos a la lista vacía
    p0.append(tmp_df['pr_0'].mean())
    # calculamos la media de fuga y agregamos a la lista vacía
    p1.append(tmp_df['pr_1'].mean())
    # agregamos el nombre del estado
    state.append(colname)
    
# convertimos a dataframe
store_pr = pd.DataFrame({'state': state, 'p0':p0, 'p1': p1}).set_index('state')
# ordenamos los valores de la probabilidad de fuga
var = store_pr['p1'].sort_values()
# graficamos
plt.figure(figsize=(10, 15))
plt.plot(var, var.index, 'o', color='orange')
# agregamos la media general de fuga
plt.axvline(var.mean())
# agregamos intervalos de confianza
for i in [1.68, 1.96]:
    plt.fill_betweenx(var.index,
    x1= var.mean() - i * np.sqrt(var.var()),
    x2 = var.mean() + i * np.sqrt(var.var()),
    alpha = 100/(.5/i))
plt.legend()
plt.title('Probabilidad de fuga de clientes a nivel estatal');