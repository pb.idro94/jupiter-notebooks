#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import seaborn as sns

# importo librerias necesarias
import missingno as msng
from sklearn.preprocessing import LabelBinarizer
import pickle
import re
#from sklearn.pipeline import Pipeline

# librerias para el entrenamiento
from sklearn.model_selection import train_test_split, GridSearchCV

# librerias de los clasificadores
from sklearn.naive_bayes import BernoulliNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier

# para medir métricas de desempeño
from sklearn.metrics import confusion_matrix, classification_report

# semilla pseudo aleatoria
rand_seed=16254

## Helpers

def infer_datatype(df, datatype, drop_none=True):
    """ A partir de un dataset y un tipo de datos entregado, devuelve los nombres de las columnas
        del dataset que tienen el correspondiente tipo de dato.

        Argumentos:
           - df: Dataframe de pandas.
           - datatype: String con el tipo de dato que se desea consultar a las columnas del dataframe.
           - drop_none: Filtra las columnas cuyo tipo de dato no esté especificado. default = True.
    """
    tmp_list = [i if df[i].dtype == datatype else None for i in df.columns]
    if drop_none is True:
        tmp_list = list(filter(lambda x: x != None, tmp_list))

    return tmp_list

def return_time_string(var, date_format='%m%d%Y'):
    return var.apply(lambda x: datetime.datetime.strptime(str(x), date_format))

def return_month_string(var):
    #var = return_time_string(var)
    return var.apply(lambda x: datetime.date(1900, x, 1).strftime('%B').upper())

def count_freq(df, selected_columns):
    """ Cuenta la cantidad de valores únicos y la frecuencia de dichos valores en las columnas
        entregadas por `selected_columns`.

        Argumentos:
            - df: dataframe que contiene las columnas en cuestión.
            - selected_columns: Columnas del dataframe de las que se quiere saber la frecuencia de valores.
    """
    return {i: df[i].unique().shape[0] for i in selected_columns}

def create_suitable_dataframe_v2(df, categorical_list = None):
    """TODO: Crea un dataframe apto para entrenamiento de acuerdo a normas básicas de limpieza de datos faltantes,
        transformación de etiquetas nulas en variables categóricas y crea atributos sinteticos de edad del sospechoso
         y conversión de distancia a sistema metrico.
    Argumentos:
        - df: Un objeto pandas.DataFrame
    returns:
    """
    ### Obtener columnas por tipo de dato
    object_data_type = infer_datatype(df, 'object')
    integer_data_type = infer_datatype(df, 'int')
    float_data_type = infer_datatype(df, 'float')

    # Quiero recuperar la lista de valores numericos tambien
    suitable_numerical_attributes = list(integer_data_type) + list(float_data_type)
    #print(suitable_numerical_attributes)

    ### Contar la cantidad de clases en el caso de las var. categóricas y frecuencia de valores para las numéricas
    object_unique_vals = count_freq(df, object_data_type)
    int_unique_vals = count_freq(df, integer_data_type)
    float_unique_vals = count_freq(df, float_data_type)

    ### Selección de atributos categoricos que cumplen con características deseadas
    suitable_categorical_attributes = dict(filter(lambda x: x[1] < 100 and x[1] >= 2, object_unique_vals.items()))
    suitable_categorical_attributes = list(suitable_categorical_attributes.keys())

    ### Reemplazo de clases faltantes
    ### {N: No, Y: Yes, U: Unknown}
    df['officrid'] = np.where(df['officrid'] == ' ', 'N', 'Y')
    df['offshld'] = np.where(df['offshld'] == ' ', 'N', 'Y')
    df['sector'] = np.where(df['sector'] == ' ', 'U', df['sector'])
    df['trhsloc'] = np.where(df['trhsloc'] == ' ', 'U', df['trhsloc'])
    df['beat'] = np.where(df['beat'] == ' ', 'U', df['beat'])
    df['offverb'] = np.where(df['offverb'] == ' ', 'N', 'Y')
    df['post'] = np.where(df['post'] == ' ', 'U', df['post'])

    meters = df['ht_feet'].astype(str) + '.' + df['ht_inch'].astype(str)
    df['meters'] = meters.apply(lambda x: float(x) * 0.3048) # Conversión de distanca a sistema metrico (non retarded)
    df['month'] = return_month_string(return_time_string(df['datestop']).apply(lambda x: x.month)) # Agregación a solo meses

    ### Calculo de la edad del suspechoso
    age_individual = return_time_string(df['dob']).apply(lambda x: 2019 - x.year)
    # Filtrar solo mayores de 18 años y menores de 100
    df['age_individual'] = np.where(np.logical_and(df['age'] > 18, df['age'] < 100), df['age'], np.nan)
    proc_df = df.dropna()
    preserve_vars = suitable_categorical_attributes + ['month', 'meters']
    if categorical_list:
        proc_df = proc_df.loc[:,categorical_list]
        preserve_vars = categorical_list
    else:
        proc_df = proc_df.loc[:, preserve_vars] # Agregar los atributos sintéticos al df
    return proc_df, preserve_vars #uitable_categorical_attributes, suitable_numerical_attributes

def graph_df(df, cols=4, w=20, h=60):
    """TODO: Permite graficar la distribución de los atributos de un dateframe
    plt.figure(figsize=(20,60))
    """
    rows = (df.shape[1] // cols)+1
    plt.figure(figsize=(w,h))
    for index, (colnames, serie) in enumerate(df.iteritems()):
        plt.subplot(rows, cols, index + 1)
        if serie.dtype == 'object':
            sns.countplot(serie.dropna())
            plt.axhline(serie.value_counts().mean(), color='forestgreen',linestyle='--')
        else:
            sns.distplot(serie.dropna(), color='slategrey')
            plt.axvline(serie.mean(), color='forestgreen', linestyle='--')
    plt.tight_layout()

def get_train_dataset(df, vec='arstmade_bin'):
    """Genera el set de datos de entrenamiento para un vector objetivo dado
    """
    X = df.drop(columns=[vec])
    y = pd.DataFrame(df[vec])

    return X, y

def set_train_datasets(df, drop_cols=['beat','post'], path='files'):
    """A partir de un dataset de entrenamiento, entrega el refinamiento aplicado
       a los datos de la prueba 2 del módulo Machine Learning.
    """

    # reutilizo la función para refinar los dataframes
    df_train_st, attr_train = create_suitable_dataframe_v2(df)

    # genero la salida de atributos para refinar los conjuntos de prueba
    attr_cat = attr_train.copy()

    # quito las variables que no usaremos
    df_train_st = df_train_st.drop(columns=drop_cols)

    # genero una lista con los atributos a binarizar
    for col in drop_cols:
        attr_train.remove(col)
    # dejamos sólo las variables categóricas
    attr_train.remove('meters')

    # genero un nuevo dataframe binarizado
    train_df_bin = pd.DataFrame(df_train_st['meters'].reset_index())
    train_df_bin = train_df_bin.drop(columns=['index'])
    # genero una lista para guardar la codificacion de cada atributo
    Encoders = []
    # recorro cada atributo
    for atr in attr_train:
        # inicializo el codificador
        encoder = LabelBinarizer(neg_label=-1, pos_label=1)
        # genero un vector con la codificacion
        X = encoder.fit_transform(df_train_st[atr].values)
        # guardo en la lista el nombre del atributo y la codificacion
        Encoders.append([atr, encoder])
        # guardo las nuevas columnas del atributo
        val_cols = list(encoder.classes_)
        # se generan los nuevos atributos
        if X.shape[1] > 2:
            # si son más de dos clases, se genera un atributo por clase
            tmp_df = pd.DataFrame(X, columns = list([atr+'_'+str(i) for i in val_cols]))
        else:
            # si se tienen dos clases, se genera solo un nuevo atributo con la variable con mayor presencia
            tmp_df = pd.DataFrame(X, columns = list([atr+'_bin']))
        # concateno al nuevo df
        train_df_bin = pd.concat([train_df_bin, tmp_df], axis=1)

    # generamos el vector objetivo relacionado con una acción violenta
    # Cuando al menos uno sea 1, se considera una acción violenta
    act_violent_attr = list(train_df_bin.filter(regex='pf_*', axis=1).columns)
    # sumo los valores de esos atributos
    train_df_bin['act_violenta_bin'] = train_df_bin[act_violent_attr].sum(axis=1)
    # si los valores son mayores a la cantidad de atributos, es una accion violenta (1)
    train_df_bin['act_violenta_bin'] = np.where(train_df_bin['act_violenta_bin'] > (len(act_violent_attr))*-1, 1, -1)
    # se descartan los atributos con los que se genera el vector objetivo
    train_df_bin = train_df_bin.drop(columns=act_violent_attr)

    # se guarda el codificador
    pickle.dump(Encoders, open(path+'/hz_test_encoder.sav', 'wb'))
    # se guarda la lista de atributos que se usaran para refinar el dataset de prueba
    pickle.dump(attr_cat, open(path+'/hz_test_attr_cat.sav', 'wb'))

    # devuelvo el dataset y la codificacion de cada atributo
    return train_df_bin

def get_test_datasets(df, drop_cols=['beat','post'], path='files'):
    """TODO
    """
    # cargo el codificador para el dataset de prueba
    Encoders = pickle.load(open(path+"/hz_test_encoder.sav", "rb"))

    # cargo la lista de atributos
    attr_cat = pickle.load(open(path+'/hz_test_attr_cat.sav', 'rb'))

    # reutilizamos la función para refinar los dataframes
    df_test_st, attr_test = create_suitable_dataframe_v2(df, attr_cat)

    # quito las variables que no usaremos
    df_test_st = df_test_st.drop(columns=drop_cols)

    # genero una lista con los atributos a binarizar
    for col in drop_cols:
        attr_test.remove(col)
    # dejamos sólo las variables categóricas
    attr_test.remove('meters')

    # genero un nuevo dataframe binarizado
    test_df_bin = pd.DataFrame(df_test_st['meters'].reset_index())
    test_df_bin = test_df_bin.drop(columns=['index'])

    # recorro cada atributo
    aux=0
    for atr in attr_test:
        if(Encoders[aux][0] == atr):
            X = Encoders[aux][1].transform(df_test_st[atr].values)
            # guardo las nuevas columnas del atributo
            val_cols = list(Encoders[aux][1].classes_)
            # se generan los nuevos atributos
            if X.shape[1] > 2:
                # si son más de dos clases, se genera un atributo por clase
                tmp_df = pd.DataFrame(X, columns = list([atr+'_'+str(i) for i in val_cols]))
            else:
                # si se tienen dos clases, se genera solo un nuevo atributo con la variable con mayor presencia
                tmp_df = pd.DataFrame(X, columns = list([atr+'_bin']))
            # concateno al nuevo df
            test_df_bin = pd.concat([test_df_bin, tmp_df], axis=1)
        else:
            print('No coinciden los atributos')
        aux+=1

    # generamos el vector objetivo relacionado con una acción violenta
    # Cuando al menos uno sea 1, se considera una acción violenta
    act_violent_attr = list(test_df_bin.filter(regex='pf_*', axis=1).columns)
    # sumo los valores de esos atributos
    test_df_bin['act_violenta_bin'] = test_df_bin[act_violent_attr].sum(axis=1)
    test_df_bin['act_violenta_bin'] = np.where(test_df_bin['act_violenta_bin'] > (len(act_violent_attr))*-1, 1, -1)

    test_df_bin = test_df_bin.drop(columns=act_violent_attr)

    # devuelvo el dataset y la codificacion de cada atributo
    return test_df_bin

def plot_final_report(model_list, yhat_list, y_test):
    """
    Reporta las metricas Accuracy, Precision, Recall y F1 dado el vector predicho y el vector real.
    Entrega es especificamente las metricas para las clases 1 y -1
    """
    acc_list = []
    n_precision_list = []
    n_recall_list = []
    n_f1_list = []

    y_precision_list = []
    y_recall_list = []
    y_f1_list = []

    for yhat in yhat_list:
        tmp_cr = classification_report(y_test, yhat, output_dict=True)

        tmp_class_n = tmp_cr['-1']
        n_precision_list.append(tmp_class_n['precision'])
        n_recall_list.append(tmp_class_n['recall'])
        n_f1_list.append(tmp_class_n['f1-score'])

        tmp_class_y = tmp_cr['1']
        y_precision_list.append(tmp_class_y['precision'])
        y_recall_list.append(tmp_class_y['recall'])
        y_f1_list.append(tmp_class_y['f1-score'])

        acc_list.append(tmp_cr['accuracy'])

    tmp_df = pd.DataFrame({
        'accuracy':acc_list,
        'n_precision':n_precision_list,
        'n_recall':n_recall_list,
        'n_f1':n_f1_list,
        'y_precision':y_precision_list,
        'y_recall':y_recall_list,
        'y_f1':y_f1_list
    }, index=model_list)

    plt.figure(figsize=(10,6))
    plt.title('Reporte Desempeño Modelos \n Procedimientos con Arresto')
    plt.plot(tmp_df['accuracy'], 'o-', label='Accuracy', c='b', )
    plt.plot(tmp_df['y_precision'], 'o-', label='Precision: 1', c='orange')
    plt.plot(tmp_df['y_recall'], 'o-', label='Recall: 1', c='g')
    plt.plot(tmp_df['y_f1'], 'o-', label='f1: 1', c='r')
    plt.axhline(tmp_df['accuracy'].mean(), ls='--', c='b', label='Main Accuracy', lw=.7)
    plt.axhline(tmp_df['y_precision'].mean(), ls='--', c='orange', label='Main Precision: 1', lw=.7)
    plt.axhline(tmp_df['y_recall'].mean(), ls='--', c='g', label='Main Recall: 1', lw=.7)
    plt.axhline(tmp_df['y_f1'].mean(), ls='--', c='r', label='Main F1: 1', lw=.7)
    #plt.ylim(.4, 1)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.tight_layout()

def plot_importance(fit_model, feat_names, attr = 30):
    """TODO: Docstring for plot_importance.

    :fit_model: TODO
    :: TODO
    :returns: TODO

    """

    tmp_importance = fit_model.feature_importances_
    sort_importance = np.argsort(tmp_importance)[::-1]
    names = [feat_names[i] for i in sort_importance]

    tmp_df = pd.DataFrame(tmp_importance[sort_importance], names, columns=['importancia']).head(attr)

    plt.title("Feature importance");
    plt.barh(tmp_df['importancia'].index, tmp_df['importancia'].values)

    return tmp_df

def tokenizador(contenido):
    """TODO
    """
    ps=nltk.stem.PorterStemmer()
    stop_words = set(stopwords.words('english'))
    temp=word_tokenize(contenido)
    filtro=[w for w in temp if not w in stop_words]
    stemmed=''
    for i in filtro:
        if i!=filtro[-1]:
            stemmed+=ps.stem(i)+' '
        else:
            stemmed+=ps.stem(i)
    return stemmed

def get_frec_by_token(df, var='lyrics', vectorizer=CountVectorizer):
    """
    Recibe un dataframe y entrega la frecuencia de las palabras.
    """
    # instancio el objeto
    vector=vectorizer(stop_words='english')
    # defino el vector que contiene los tokens
    token = df[var]
    # aplico fit_transform
    vector_fit = vector.fit_transform(token)
    # genero los token (palabras)
    words = vector.get_feature_names()
    # genero la frecuencia de cada token
    words_freq = vector_fit.toarray().sum(axis=0)
    # genero un dataframe
    df_freq = pd.DataFrame(data=words_freq, index=words, columns=['frecuencia'])
    # retorno el dataframe ordenado por frecuencia
    return df_freq.sort_values(by='frecuencia', ascending=False)

def ref_twits(df):
    """TODO
    """
    # para refinar los datos vamos a transformar los twits a lowercase
    df['content'] = df['content'].str.lower()

    # refactorizaremos nuestro vector objetivo para obtener si el sentiemiento es positivo o negativo
    df_obj = df['sentiment']

    # utilizaremos la siguiente lista
    sentiment = [('worry',-1),
                ('happiness',1),
                ('sadness',-1),
                ('love',1),
                ('surprise',1),
                ('fun',1),
                ('relief',1),
                ('hate',-1),
                ('empty',-1),
                ('enthusiasm',1),
                ('boredom',-1),
                ('anger',-1),
                ('neutral',0)]

    # refactorizamos sentiment
    df['sentiment_bin'] = df['sentiment']
    for i in range(len(sentiment)):
        df['sentiment_bin'] = df['sentiment_bin'].replace(str(sentiment[i][0]),int(sentiment[i][1]))

    # Refactorizamos el vector objetivo para elegir aleatoriamente los sentimientos neutrales (0) entre positivos (1) y negativos (-1)
    random.seed(rand_seed)
    for i in df[df['sentiment_bin'] == 0].index:
        df['sentiment_bin'][i] = random.choice([-1,1])

    # ahora refactorizaremos el contenido de los twits para no considerar palabras no representativas
    stopwords = nltk.download('stopwords')
    nltk.download('wordnet')

    # limpiamos la columna content de caracteres extraños

    # elimina http
    df['content']=df['content'].map(lambda x:re.sub(r"(?:\@|https?\://)\S+", "",x))

    #Reemplazar contracciones:
    df['content']=df['content'].map(lambda x:re.sub(r"\'re"," are", x))
    df['content']=df['content'].map(lambda x:re.sub(r"n\'t"," not", x))
    df['content']=df['content'].map(lambda x:re.sub(r"\'m"," am", x))
    df['content']=df['content'].map(lambda x:re.sub(r"\'s"," is", x))

    #Eliminar username:
    df['content']=df['content'].map(lambda x:re.sub(r'(\A|\s)@(\w+)',"", x))

    #Eliminar símbolos
    df['content']=df['content'].map(lambda x:re.sub(r"[,+]|[:+]|[?+]|[!+]|[-_]|[()]|[=]|[.+]|[/+]|['/]|[&+]|[;+]","", x))

    # para lemantización utilizaremos la siguiente lógica
    nltk.download('stopwords')

    from nltk.stem import PorterStemmer
    from nltk.corpus import stopwords
    from nltk.tokenize import word_tokenize

    df['content_token']=df['content'].apply(tokenizador)

    return df

def set_twit_train(X, path='files'):
    # Aplico vectorizador a los conjuntos de entrenamiento y test
    vector = TfidfVectorizer(strip_accents='ascii')
    X_train = vector.fit_transform(X)

    # se guarda el codificador
    pickle.dump(vector, open(path+'/hz_twit_vector.sav', 'wb'))

    return X_train

def set_twit_test(X, path='files')

    # cargo el vectorizador para el dataset de prueba
    vector = pickle.load(open(path+"/hz_twit_vector.sav", "rb"))

    # ahora tranformamos el dataset de prueba
    X_test = vector.transform(X)

    return X_test
