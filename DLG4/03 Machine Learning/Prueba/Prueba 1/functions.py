#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import seaborn as sns

# importo librerias necesarias
import missingno as msng
from sklearn.preprocessing import LabelBinarizer
import pickle
import re
import random
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

#contar ocurrencias
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split

# para medir métricas de desempeño
from sklearn.metrics import confusion_matrix, classification_report

# semilla pseudo aleatoria
rand_seed=16254

## Helpers

def plot_final_report(model_list, yhat_list, y_test):
    """
    Reporta las metricas Accuracy, Precision, Recall y F1 dado el vector predicho y el vector real.
    Entrega es especificamente las metricas para las clases 1 y -1
    """
    acc_list = []
    n_precision_list = []
    n_recall_list = []
    n_f1_list = []

    y_precision_list = []
    y_recall_list = []
    y_f1_list = []

    for yhat in yhat_list:
        tmp_cr = classification_report(y_test, yhat, output_dict=True)

        tmp_class_n = tmp_cr['-1']
        n_precision_list.append(tmp_class_n['precision'])
        n_recall_list.append(tmp_class_n['recall'])
        n_f1_list.append(tmp_class_n['f1-score'])

        tmp_class_y = tmp_cr['1']
        y_precision_list.append(tmp_class_y['precision'])
        y_recall_list.append(tmp_class_y['recall'])
        y_f1_list.append(tmp_class_y['f1-score'])

        acc_list.append(tmp_cr['accuracy'])

    tmp_df = pd.DataFrame({
        'accuracy':acc_list,
        'n_precision':n_precision_list,
        'n_recall':n_recall_list,
        'n_f1':n_f1_list,
        'y_precision':y_precision_list,
        'y_recall':y_recall_list,
        'y_f1':y_f1_list
    }, index=model_list)

    plt.figure(figsize=(10,6))
    plt.title('Reporte Desempeño Modelos \n Análisis de sentiemiento')
    plt.plot(tmp_df['accuracy'], 'o-', label='Accuracy', c='b', )
    plt.plot(tmp_df['y_precision'], 'o-', label='Precision: 1', c='orange')
    plt.plot(tmp_df['y_recall'], 'o-', label='Recall: 1', c='g')
    plt.plot(tmp_df['y_f1'], 'o-', label='f1: 1', c='r')
    plt.axhline(tmp_df['accuracy'].mean(), ls='--', c='b', label='Main Accuracy', lw=.7)
    plt.axhline(tmp_df['y_precision'].mean(), ls='--', c='orange', label='Main Precision: 1', lw=.7)
    plt.axhline(tmp_df['y_recall'].mean(), ls='--', c='g', label='Main Recall: 1', lw=.7)
    plt.axhline(tmp_df['y_f1'].mean(), ls='--', c='r', label='Main F1: 1', lw=.7)
    #plt.ylim(.4, 1)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.tight_layout()

def plot_importance(fit_model, feat_names, attr = 30):
    """TODO: Docstring for plot_importance.

    :fit_model: TODO
    :: TODO
    :returns: TODO

    """

    tmp_importance = fit_model.feature_importances_
    sort_importance = np.argsort(tmp_importance)[::-1]
    names = [feat_names[i] for i in sort_importance]

    tmp_df = pd.DataFrame(tmp_importance[sort_importance], names, columns=['importancia']).head(attr)

    plt.title("Feature importance");
    plt.barh(tmp_df['importancia'].index, tmp_df['importancia'].values)

    return tmp_df

def tokenizador(contenido):
    """TODO
    """
    ps=nltk.stem.PorterStemmer()
    stop_words = set(stopwords.words('english'))
    temp=word_tokenize(contenido)
    filtro=[w for w in temp if not w in stop_words]
    stemmed=''
    for i in filtro:
        if i!=filtro[-1]:
            stemmed+=ps.stem(i)+' '
        else:
            stemmed+=ps.stem(i)
    return stemmed

def get_frec_by_token(df, var='lyrics', vectorizer=CountVectorizer):
    """
    Recibe un dataframe y entrega la frecuencia de las palabras.
    """
    # instancio el objeto
    vector=vectorizer(stop_words='english')
    # defino el vector que contiene los tokens
    token = df[var]
    # aplico fit_transform
    vector_fit = vector.fit_transform(token)
    # genero los token (palabras)
    words = vector.get_feature_names()
    # genero la frecuencia de cada token
    words_freq = vector_fit.toarray().sum(axis=0)
    # genero un dataframe
    df_freq = pd.DataFrame(data=words_freq, index=words, columns=['frecuencia'])
    # retorno el dataframe ordenado por frecuencia
    return df_freq.sort_values(by='frecuencia', ascending=False)

def ref_twits(df):
    """TODO
    """
    # para refinar los datos vamos a transformar los twits a lowercase
    df['content'] = df['content'].str.lower()

    # refactorizaremos nuestro vector objetivo para obtener si el sentiemiento es positivo o negativo
    df_obj = df['sentiment']

    # utilizaremos la siguiente lista
    sentiment = [('worry',-1),
                ('happiness',1),
                ('sadness',-1),
                ('love',1),
                ('surprise',1),
                ('fun',1),
                ('relief',1),
                ('hate',-1),
                ('empty',-1),
                ('enthusiasm',1),
                ('boredom',-1),
                ('anger',-1),
                ('neutral',0)]

    # refactorizamos sentiment
    df['sentiment_bin'] = df['sentiment']
    for i in range(len(sentiment)):
        df['sentiment_bin'] = df['sentiment_bin'].replace(str(sentiment[i][0]),int(sentiment[i][1]))

    # Refactorizamos el vector objetivo para elegir aleatoriamente los sentimientos neutrales (0) entre positivos (1) y negativos (-1)
    random.seed(rand_seed)
    for i in df[df['sentiment_bin'] == 0].index:
        df['sentiment_bin'][i] = random.choice([-1,1])

    # ahora refactorizaremos el contenido de los twits para no considerar palabras no representativas
    stopwords = nltk.download('stopwords')
    nltk.download('wordnet')

    # limpiamos la columna content de caracteres extraños

    # elimina http
    df['content']=df['content'].map(lambda x:re.sub(r"(?:\@|https?\://)\S+", "",x))

    #Reemplazar contracciones:
    df['content']=df['content'].map(lambda x:re.sub(r"\'re"," are", x))
    df['content']=df['content'].map(lambda x:re.sub(r"n\'t"," not", x))
    df['content']=df['content'].map(lambda x:re.sub(r"\'m"," am", x))
    df['content']=df['content'].map(lambda x:re.sub(r"\'s"," is", x))

    #Eliminar username:
    df['content']=df['content'].map(lambda x:re.sub(r'(\A|\s)@(\w+)',"", x))

    #Eliminar símbolos
    df['content']=df['content'].map(lambda x:re.sub(r"[,+]|[:+]|[?+]|[!+]|[-_]|[()]|[=]|[.+]|[/+]|['/]|[&+]|[;+]","", x))

    # para lemantización utilizaremos la siguiente lógica
    nltk.download('stopwords')

    from nltk.stem import PorterStemmer
    from nltk.corpus import stopwords
    from nltk.tokenize import word_tokenize

    df['content_token']=df['content'].apply(tokenizador)

    return df

def set_twit_train(X, path='files'):
    # Aplico vectorizador a los conjuntos de entrenamiento y test
    vector = TfidfVectorizer(strip_accents='ascii')
    X_train = vector.fit_transform(X)

    # se guarda el codificador
    pickle.dump(vector, open(path+'/hz_twit_vector.sav', 'wb'))

    return X_train

def set_twit_test(X, path='files'):

    # cargo el vectorizador para el dataset de prueba
    vector = pickle.load(open(path+"/hz_twit_vector.sav", "rb"))

    # ahora tranformamos el dataset de prueba
    X_test = vector.transform(X)

    return X_test

def get_twit_train(df, attr='content_token', y='sentiment_bin'):
    X_train, X_test, y_train, y_test = train_test_split(df[attr],
                                                        df[y],
                                                        test_size=.33,
                                                        random_state=rand_seed)
    return X_train, X_test, y_train, y_test
