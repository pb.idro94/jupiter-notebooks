import pandas as pd
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt


def ver_data_perdida(dataframe,var,print_list = False):
    
    cantidad_data_perdida = dataframe[var].isnull().sum()
    porcentaje_data_perdida = cantidad_data_perdida/len(dataframe[var])
    print("\n Columna:\t\t\t{} \n Cantidad de datos perdidos: \t{} \n Frecuencia:\t\t\t{:.2%}".format(var,cantidad_data_perdida,porcentaje_data_perdida))
    
    if print_list:
        print(' Paises con datos perdidos :\t', dataframe.loc[dataframe[var].isnull()]['cname'].unique())

    print('___________________________________________________________________________________________________________________')

def medias_descriptivas(dataframe): 
    for colname, colserie in dataframe.iteritems():
        if pd.api.types.is_numeric_dtype(colserie):
            print(colname)
            print(colserie.describe())
        else:
            print(colname)
            print(colserie.describe())
        print('\n')            
        
def graficar_histograma(dataframe, var, sample_mean = False, true_mean = False):
    plt.grid()
    df = pd.read_csv("qog_std_cs_jan18.csv")
    df_dropna = df[var].dropna()
    df_dropna_gb = dataframe[var].dropna()
    media = df_dropna.mean()
    media_grupo = df_dropna_gb.mean()
    plt.hist(df_dropna_gb, color='grey', alpha=.4, density=True)
    
    x_min, x_max = plt.xlim()
    x_axis = np.linspace(x_min, x_max, 100)
    gauss_kde = stats.gaussian_kde(df_dropna) # Kernel gaussiano
    Z = np.reshape(gauss_kde(x_axis).T, x_axis.shape)
    plt.plot(x_axis, Z, color='tomato', lw=3)
    plt.axvline(df_dropna.mean(), color='dodgerblue', linestyle='--', lw=3)
    plt.title("Histograma del IDH")
    plt.subplot(2, 1, 2)
    plt.hist(np.random.normal(0.696, np.sqrt(0.024), 1000), color='grey', alpha=.4, density=True)
    plt.xlim(0.3, 1)
    x_axis = np.linspace(.3, 1, 1000) 
    plt.plot(x_axis, stats.norm.pdf(x_axis,0.696, np.sqrt(0.024)), color='tomato', lw=3)
    plt.axvline(0.696, color='dodgerblue', linestyle='--', lw=3) 
    plt.title("Distribución simulada del IDH") 
    plt.subplots_adjust(hspace=0.6)
    
    if (sample_mean):
        plt.axvline(media_grupo, color = 'tomato', lw=3, linestyle = '-', label="sub df median" )
    
    if (true_mean):    
        plt.axvline(media, color = 'blue', lw=3, linestyle = '-', label="df median")
    
    plt.title("Histograma de {}".format(var))    
    plt.legend(loc='upper right')
    
    if (media_grupo > media):
        print('En la variable {} la media de la submuestra SI es mayor a la media de la muestra completa'.format(var))
    else:
        print('En la variable {} la media de la submuestra NO es mayor a la media de la muestra completa'.format(var))
     



def graficar_dotplot(dataframe, plot_var, plot_by, global_stat = False, statistic = 'mean'):
    plt.grid()
    plt.xlabel(plot_var)
    plt.ylabel(plot_by)
    if statistic == 'mean':
        plot = dataframe.groupby(plot_by)[plot_var].mean()
    elif statistic == 'median':
        plot = dataframe.groupby(plot_by)[plot_var].median()
    plt.plot(plot.values, plot.index, 'x')
    if global_stat:
        if statistic == 'mean':
            plt.axvline(dataframe[plot_var].mean(), color='green', linestyle='-')
            print(dataframe[plot_var].mean(), dataframe[plot_var].median())
        elif statistic == 'median':
            plt.axvline(dataframe[plot_var].median(), color='green', linestyle='-')
            